package com.qitsoft.texgames.core.config;

import java.io.File;

/**
 * Created by ssolovio on 31.12.13.
 */
public final class Constants {

    private Constants() {
    }

    public static final int PAGE_SIZE = 10;

    public static final class Directories {

        private Directories() {
        }

        public static final File USER_HOME = new File(System.getProperty("user.home"));

        public static final File SITE_HOME = new File(USER_HOME, "texgames");

        public static final File TEMP_DIR = new File(SITE_HOME, "tmp");

        public static final File IMAGES_DIR = new File(SITE_HOME, "images");

        static {
            USER_HOME.mkdirs();
            SITE_HOME.mkdirs();
            TEMP_DIR.mkdirs();
            IMAGES_DIR.mkdirs();
        }
    }
}
