package com.qitsoft.texgames.core.features.pages;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.qitsoft.texgames.core.features.CukeConfig;
import com.qitsoft.texgames.core.features.elements.*;
import com.qitsoft.texgames.core.features.elements.support.ExtendedFieldDecorator;
import com.qitsoft.texgames.core.features.utils.CukePredicates;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ssolovio on 26.12.13.
 */
public class Page {

    private WebDriver webDriver;
    private CukeConfig config;
    private WebElement content;

    @FindBy(id = "mainmenu")
    @WebContext("top")
    private Menu menu;

    @FindBy(css = "div.modal")
    private List<Dialog> dialogs;

    @FindBy(tagName = "h1")
    private Heading h1;

    @FindBy(css = "#top-message")
    private WebElement topMessage;

    public Page(WebDriver webDriver, CukeConfig config) {
        this.webDriver = webDriver;
        this.config = config;

        content = webDriver.findElement(By.id("content"));

        Map<String, SearchContext> searchContexts = new HashMap<>();
        addContexts(searchContexts);
        searchContexts.put("top", webDriver);

        PageFactory.initElements(new ExtendedFieldDecorator(webDriver, content, searchContexts), this);
    }

    public WebElement getContentElement() {
        return content;
    }

    public CukeConfig getConfig() {
        return config;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public Menu getMenu() {
        return menu;
    }

    public String getTitle() {
        return webDriver.getTitle();
    }

    public Heading getH1() {
        return h1;
    }

    public String getTopMessage() {
        try {
            String classes = topMessage.getAttribute("class");
            Matcher m = Pattern.compile("\\balert-(.*)\\b").matcher(classes);
            m.find();

            return topMessage.getText().trim();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public String getTopMessageStatus() {
        try {
            String classes = topMessage.getAttribute("class");
            Matcher m = Pattern.compile("\\balert-(.*)\\b").matcher(classes);
            m.find();

            return m.group(1);
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public String getFullRawUrl() {
        return webDriver.getCurrentUrl();
    }

    public URL getFullUrl() throws MalformedURLException {
        return new URL(webDriver.getCurrentUrl());
    }

    public String getPageAddress() {
        try {
            return getFullUrl().getPath().replaceFirst("^" + Pattern.quote(config.getContextPath()), "");
        } catch (MalformedURLException e) {
            return "";
        }
    }

    protected void addContexts(Map<String, SearchContext> contexts) {
    }

    public List<Dialog> getDialogs() {
        return dialogs;
    }

    public Dialog waitOpeningDialog() {
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        return webDriverWait.until(new Function<WebDriver, Dialog>() {
            @Override
            public Dialog apply(WebDriver input) {
                Collection<Dialog> foundDialogs = Collections2.filter(dialogs, CukePredicates.isVisible());
                Iterator<Dialog> iterator = foundDialogs.iterator();

                if (iterator.hasNext()) {
                    return iterator.next();
                } else {
                    return null;
                }
            }
        });
    }

    public boolean waitClosingDialog() {
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        return webDriverWait.until(new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver input) {
                Collection<Dialog> foundDialogs = Collections2.filter(dialogs, CukePredicates.isVisible());
                Iterator<Dialog> iterator = foundDialogs.iterator();

                return !iterator.hasNext();
            }
        });
    }

    public Dialog getActiveDialog() {
        Collection<Dialog> foundDialogs = Collections2.filter(dialogs, CukePredicates.isVisible());
        Iterator<Dialog> iterator = foundDialogs.iterator();

        if (iterator.hasNext()) {
            return iterator.next();
        } else {
            return null;
        }
    }

    @SafeVarargs
    public final WebElement findContentElement(By by, Predicate<WebElement>... predicates) {
        List<WebElement> elements = getContentElement().findElements(by);
        Iterator<WebElement> iterator = Collections2.filter(elements, Predicates.and(predicates)).iterator();
        if (iterator.hasNext()) {
            return iterator.next();
        } else {
            return null;
        }
    }

    @SafeVarargs
    public final Collection<WebElement> findContentElements(By by, Predicate<WebElement>... predicates) {
        return Collections2.filter(getContentElement().findElements(by), Predicates.and(predicates));
    }

    @SafeVarargs
    public final Button findContentButton(String name, Predicate<Button>... predicates) {
        Collection<WebElement> elements = findContentElements(By.cssSelector(".btn"), CukePredicates.isButton(name));
        Iterator<Button> iterator = Collections2.filter(
                Collections2.transform(elements, new Function<WebElement, Button>() {
                    @Override
                    public Button apply(WebElement input) {
                        return new Button(getWebDriver(), input);
                    }
                }),
                Predicates.and(predicates)
        ).iterator();

        if (iterator.hasNext()) {
            return iterator.next();
        } else {
            return null;
        }
    }
}
