package com.qitsoft.texgames.core.features.pages;

import com.qitsoft.texgames.core.features.CukeConfig;
import com.qitsoft.texgames.core.features.elements.Paginator;
import com.qitsoft.texgames.core.features.elements.Table;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * Created by ssolovio on 25.12.13.
 */
public class TablePage extends Page {

    @FindBy(tagName = "table")
    private Table table;

    @FindBy(css = "ul.pagination")
    private Paginator paginator;

    public TablePage(WebDriver webDriver, CukeConfig config) {
        super(webDriver, config);
    }

    public Table getTable() {
        return table;
    }

    public Paginator getPaginator() {
        return paginator;
    }
}
