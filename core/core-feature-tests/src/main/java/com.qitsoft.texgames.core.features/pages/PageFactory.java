package com.qitsoft.texgames.core.features.pages;

import com.qitsoft.texgames.core.features.CukeConfig;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by ssolovio on 25.12.13.
 */
@Component
@Scope("cucumber-glue")
public class PageFactory {

    @Autowired
    private WebDriver webDriver;

    @Autowired
    private CukeConfig config;

    public <T> T createPage(Class<? extends T> pageClass) {
        try {
            return pageClass.getConstructor(WebDriver.class, CukeConfig.class).newInstance(webDriver, config);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

}
