package com.qitsoft.texgames.core.features.pages;

import com.qitsoft.texgames.core.features.CukeConfig;
import com.qitsoft.texgames.core.features.elements.Form;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * Created by ssolovio on 09.01.14.
 */
public class FormPage extends Page {

    @FindBy(css = "form[role='form']")
    private Form form;

    public FormPage(WebDriver webDriver, CukeConfig config) {
        super(webDriver, config);
    }

    public Form getForm() {
        return form;
    }
}
