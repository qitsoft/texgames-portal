package com.qitsoft.texgames.core.features;

/**
 * Created by ssolovio on 25.12.13.
 */
public class CukeConfig {

    private String contextPath;

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }
}
