package com.qitsoft.texgames.core.features;

import com.qitsoft.texgames.core.config.Constants;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by ssolovio on 22.12.13.
 */
@ContextConfiguration(locations = "classpath:featureTestsContext.xml")
public class CreateImagesHook {

    @Before
    private void initImages() throws IOException {
        createImageAndThumb("img_1");
        createImageAndThumb("img_2");
        createImageAndThumb("img_3");
    }

    private void createImageAndThumb(String imageId) throws IOException {
        Path imagePath = Paths.get(Constants.Directories.IMAGES_DIR.getAbsolutePath(), imageId + ".jpeg");
        Path thumbPath = Paths.get(Constants.Directories.IMAGES_DIR.getAbsolutePath(), imageId + "-thumb.jpeg");

        Files.deleteIfExists(imagePath);
        Files.deleteIfExists(thumbPath);

        InputStream morskoyBoyStream = getClass().getClassLoader().getResourceAsStream("assets/morskoy-boy.jpeg");
        InputStream morskoyBoyThumbPathStream = getClass().getClassLoader().getResourceAsStream("assets/morskoy-boy-thumb.jpeg");

        Files.copy(morskoyBoyStream, imagePath);
        Files.copy(morskoyBoyThumbPathStream, thumbPath);
    }

}
