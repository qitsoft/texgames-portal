package com.qitsoft.texgames.core.features;

import liquibase.Liquibase;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by ssolovio on 28.12.13.
 */
public class DatabaseManager {

    protected final Logger logger = Logger.getLogger(this.getClass().getName());

    private SessionFactory sessionFactory;

    private Locale[] locales = new Locale[0];

    private Properties entities = new Properties();

    private Session session = null;

    @Autowired
    private DataSource testDataSource;

    @PostConstruct
    private void init() {
        initEntitiesForLocale(null);

        for (Locale locale : locales) {
            initEntitiesForLocale(locale);
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setUpDatabase() throws SQLException, LiquibaseException {
        DatabaseConnection databaseConnection = new JdbcConnection(testDataSource.getConnection());
        ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor(getClass().getClassLoader());
        Liquibase liquibase = new Liquibase("master.xml", resourceAccessor, databaseConnection);

        liquibase.dropAll();
        liquibase.update("test");

        liquibase.getDatabase().close();
    }

    private void initEntitiesForLocale(Locale locale) {
        if (locale == null) {
            loadFromResource("entities.xml");
        } else {
            Locale searchLocale = new Locale(locale.getLanguage(), locale.getCountry(), locale.getVariant());
            loadFromResource("entities_"+searchLocale.toString()+".xml");

            searchLocale = new Locale(locale.getLanguage(), locale.getCountry());
            loadFromResource("entities_"+searchLocale.toString()+".xml");

            searchLocale = new Locale(locale.getLanguage());
            loadFromResource("entities_"+searchLocale.toString()+".xml");
        }
    }

    private void loadFromResource(String resource) {
        InputStream in = getClass().getClassLoader().getResourceAsStream(resource);
        if (in != null) {
            try {
                entities.loadFromXML(in);
            } catch (IOException e) {
                logger.log(Level.FINE, e.getLocalizedMessage(), e);
            }
        }
    }

    public Session getSession() {
        sessionFactory.getCache().evictEntityRegions();
        sessionFactory.getCache().evictCollectionRegions();
        sessionFactory.getCache().evictDefaultQueryRegion();
        session.clear();
        return session;
    }

    public void openSession() {
        session = sessionFactory.openSession();
    }

    public void closeSession() {
        session.close();
    }

    public Properties getEntities() {
        return entities;
    }

    public Class getEntityClass(String entityName) throws ClassNotFoundException {
        String entityClassName = entities.getProperty(entityName);
        if (StringUtils.isNotBlank(entityClassName)) {
            return getClass().forName(entityClassName);
        }
        return null;
    }
}
