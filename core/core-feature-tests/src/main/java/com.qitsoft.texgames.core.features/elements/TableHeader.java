package com.qitsoft.texgames.core.features.elements;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.qitsoft.texgames.core.features.utils.CukeFunctions;
import com.qitsoft.texgames.core.features.utils.CukePredicates;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by ssolovio on 25.12.13.
 */
public class TableHeader extends AbstractContainer {

    @FindBy(tagName = "th")
    private List<WebElement> cellElements;

    public TableHeader(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public List<WebElement> getCellElements() {
        return cellElements;
    }

    public List<String> getTextCells() {
        return Lists.transform(cellElements, CukeFunctions.elementToTextTransformer);
    }

    public List<String> getSignificantTitles() {
        return Lists.newArrayList(Iterables.filter(
                Lists.transform(cellElements, CukeFunctions.elementToTextTransformer),
                CukePredicates.significantTitlePredicate));
    }


}
