package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 25.12.13.
 */
public abstract class AbstractElement implements Element {

    private WebElement wrappedElement;

    private WebDriver webDriver;

    public AbstractElement(WebDriver webDriver, WebElement wrappedElement) {
        this.wrappedElement = wrappedElement;
        this.webDriver = webDriver;
    }

    public WebElement getWrappedElement() {
        return wrappedElement;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public JavascriptExecutor getJS() {
        return (JavascriptExecutor) webDriver;
    }

    @Override
    public String getId() {
        return wrappedElement.getAttribute("id");
    }

    @Override
    public boolean isVisible() {
        return wrappedElement.isDisplayed();
    }

    @Override
    public boolean isEnabled() {
        return wrappedElement.isEnabled();
    }
}
