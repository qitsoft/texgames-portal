package com.qitsoft.texgames.core.features.elements;

import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by ssolovio on 30.12.13.
 */
public class Paginator extends AbstractContainer {

    @FindBy(tagName = "li")
    private List<WebElement> navigatorItems;

    public Paginator(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public void navitageToPage(int num) {
        navigatorItems.get(num).findElement(By.tagName("a")).click();
    }

    public void navitageToNextPage() {
        navigatorItems.get(navigatorItems.size()-1).findElement(By.tagName("a")).click();
    }

    public void navitageToPrevPage() {
        navigatorItems.get(0).findElement(By.tagName("a")).click();
    }

    public int getActivePage() {
        int i = 0;
        for (WebElement element : navigatorItems) {
            if (ArrayUtils.contains(element.getAttribute("class").split("\\s+"), "active") ) {
                return i;
            }
            i++;
        }
        return -1;
    }
}
