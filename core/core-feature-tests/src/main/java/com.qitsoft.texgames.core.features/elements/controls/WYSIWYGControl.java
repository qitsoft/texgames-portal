package com.qitsoft.texgames.core.features.elements.controls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 09.01.14.
 */
public class WYSIWYGControl extends FieldControl<String> {

    private WebElement editorFrame;

    public WYSIWYGControl(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);

        editorFrame = wrappedElement.findElement(By.xpath(String.format("..//iframe[@id='%s_ifr']", wrappedElement.getAttribute("id"))));
    }

    @Override
    public void clear() {
        getJS().executeScript(String.format("$('#%s').val('');", getId()));

        getWebDriver().switchTo().frame(getId()+"_ifr");
        getJS().executeScript(String.format("document.body.innerHTML = '';"));
        getWebDriver().switchTo().defaultContent();
    }

    @Override
    public String getValue() {
        getWebDriver().switchTo().frame(getId()+"_ifr");
        String result  = getWebDriver().findElement(By.tagName("body")).getText();
        getWebDriver().switchTo().defaultContent();

        return result;
    }

    @Override
    public void setValue(String value) {
        getWebDriver().switchTo().frame(getId()+"_ifr");
        getWebDriver().findElement(By.tagName("body")).sendKeys(value);
        getWebDriver().switchTo().defaultContent();
    }
}
