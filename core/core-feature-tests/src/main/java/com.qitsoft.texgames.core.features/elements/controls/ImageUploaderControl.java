package com.qitsoft.texgames.core.features.elements.controls;

import com.google.common.base.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * Created by ssolovio on 09.01.14.
 */
public class ImageUploaderControl extends FieldControl<Object> {

    private WebElement button;

    private WebElement hiddenElement;

    private WebElement imageElement;

    public ImageUploaderControl(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);

        String hiddenId = getId().replace("-file", "");
        this.hiddenElement = getWebDriver().findElement(By.id(hiddenId));
        this.imageElement = getWebDriver().findElement(By.id("image-" + hiddenId));
//        this.button = getWrappedElement().findElement(By.xpath(String.format("../../button[@id='btn-%s']", getId())));
    }

    @Override
    public void clear() {
    }

    @Override
    public String getValue() {
        return hiddenElement.getAttribute("value");
    }

    @Override
    public void setValue(String imageName) {
        try {
            Path tmpImagePath = Files.createTempFile("featureTests", "."+StringUtils.substringAfterLast(imageName, "."));
            Files.copy(getClass().getClassLoader().getResourceAsStream("assets/" + imageName), tmpImagePath, StandardCopyOption.REPLACE_EXISTING);
            getWrappedElement().sendKeys(tmpImagePath.toAbsolutePath().toString());

            new WebDriverWait(getWebDriver(), 30).until(new Predicate<WebDriver>() {
                @Override
                public boolean apply(WebDriver input) {
                    return getValue() != null && !getValue().isEmpty();
                }
            });

            Files.delete(tmpImagePath);
        } catch (IOException e) {
            throw new RuntimeException("Unable to create temporary file to store the image.", e);
        }
    }

    public String getImageId() {
        String src = imageElement.getAttribute("src");
        if (StringUtils.isBlank(src)) {
            return null;
        }

        return StringUtils.substringAfterLast(src, "/").replaceFirst("-thumb\\.jpeg$", "");
    }

    public WebElement getImageElement() {
        return imageElement;
    }

    public WebElement getHiddenElement() {
        return hiddenElement;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
