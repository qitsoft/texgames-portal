package com.qitsoft.texgames.core.features.elements.controls;

import com.qitsoft.texgames.core.features.elements.AbstractElement;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 09.01.14.
 */
public abstract class FieldControl<T> extends AbstractElement {

    public FieldControl(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public void clear() {
        getWrappedElement().clear();
    }

    public void setOrClearValue(String value) {
        if (StringUtils.isNotBlank(value)) {
            setValue(value);
        } else {
            clear();
        }
    }

    public String getValue() {
        return getWrappedElement().getAttribute("value");
    }

    public void setValue(String value) {
        getWrappedElement().sendKeys(value);
    }

    public boolean isFocused(WebDriver webDriver) {
        return getWrappedElement().equals(webDriver.switchTo().activeElement());
    }
}
