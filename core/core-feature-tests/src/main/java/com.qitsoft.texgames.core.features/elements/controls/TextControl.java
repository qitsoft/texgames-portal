package com.qitsoft.texgames.core.features.elements.controls;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 09.01.14.
 */
public class TextControl extends FieldControl<String> {

    public TextControl(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

}
