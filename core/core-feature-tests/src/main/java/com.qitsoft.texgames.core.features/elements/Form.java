package com.qitsoft.texgames.core.features.elements;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Iterator;
import java.util.List;

/**
 * Created by ssolovio on 09.01.14.
 */
public class Form extends AbstractContainer {

    @FindBy(css = "div.form-group, div.checkbox, div.radio")
    private List<FormField> fields;

    @FindBy(css = "button.btn.btn-primary")
    private Button submit;

    public Form(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public List<FormField> getFields() {
        return fields;
    }

    public FormField getFielsByLabel(final String label) {
        Iterator<FormField> iterator = Collections2.filter(getFields(), new Predicate<FormField>() {
            @Override
            public boolean apply(FormField input) {
                return StringUtils.containsIgnoreCase(input.getLabel(), label);
            }
        }).iterator();

        if (iterator.hasNext()) {
            return iterator.next();
        } else {
            return null;
        }
    }

    public Button getSubmit() {
        return submit;
    }

    public void submit() {
        ((JavascriptExecutor) getWebDriver()).executeScript(String.format("$('#%s').submit();", getId()));
    }
}
