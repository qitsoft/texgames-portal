package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 03.01.14.
 */
public class Button extends AbstractElement {

    public Button(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public String getText() {
        return getWrappedElement().getText().trim();
    }

    public void click() {
        getWrappedElement().click();
    }

}
