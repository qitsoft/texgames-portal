package com.qitsoft.texgames.core.features.elements;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.qitsoft.texgames.core.features.elements.controls.FieldControl;
import com.qitsoft.texgames.core.features.elements.controls.ImageUploaderControl;
import com.qitsoft.texgames.core.features.elements.controls.TextControl;
import com.qitsoft.texgames.core.features.elements.controls.WYSIWYGControl;
import com.qitsoft.texgames.core.features.utils.CukeFunctions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by ssolovio on 09.01.14.
 */
public class FormField extends AbstractContainer {

    @FindBy(tagName = "label")
    private WebElement label;

    @FindAll({
            @FindBy(css = "input"),
            @FindBy(css = "textarea")
    })
    private WebElement controlElement;

    @FindBy(css = ".help-block")
    private WebElement helpBlockElement;

    @FindBy(css = ".help-block > ul > li")
    private List<WebElement> errorMessageElements;

    private FieldControl control;

    public FormField(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public String getLabel() {
        return label.getText().replaceFirst("\\*$", "").trim();
    }

    public String getHelpBlock() {
        return helpBlockElement.getText();
    }

    public List<String> getErrorMessages() {
        return Lists.transform(errorMessageElements, CukeFunctions.elementToTextTransformer);
    }

    public WebElement getControlElement() {
        return controlElement;
    }

    public WebElement getHelpBlockElement() {
        return helpBlockElement;
    }

    public List<WebElement> getErrorMessageElements() {
        return errorMessageElements;
    }

    public void waitToAppearErrorMessages() {
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), 10);
        webDriverWait.until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver input) {
                return !getErrorMessages().isEmpty();
            }
        });
    }

    public FieldControl getControl() {
        if (control == null) {
            switch (controlElement.getTagName()) {
                case "input":
                    String type = controlElement.getAttribute("type");
                    switch (type) {
                        case "file": control = new ImageUploaderControl(getWebDriver(), controlElement); break;
                        case "text":
                        default: control = new TextControl(getWebDriver(), controlElement); break;
                    }
                    break;
                case "textarea":
                    String classNames = controlElement.getAttribute("class");
                    if (classNames == null) {
                        control = null;
                    } else if (classNames.contains("wysiwyg")) {
                        control = new WYSIWYGControl(getWebDriver(), controlElement);
                    }
                    break;
            }
        }
        return control;
    }

    @Override
    public String toString() {
        return String.format("FormField \"%s\" with control [%s]", getLabel(), getControl().toString());
    }
}
