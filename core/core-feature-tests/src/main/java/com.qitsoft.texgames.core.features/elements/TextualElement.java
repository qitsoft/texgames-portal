package com.qitsoft.texgames.core.features.elements;

/**
 * Created by ssolovio on 25.12.13.
 */
public interface TextualElement extends Element {

    String getText();

}
