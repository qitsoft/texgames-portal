package com.qitsoft.texgames.core.features.elements.support;

import com.qitsoft.texgames.core.features.elements.Element;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by ssolovio on 25.12.13.
 */
public class DefaultElementFactory implements ElementFactory {

    @Override
    public <E extends Element> E create(final Class<E> elementClass, final WebDriver webDriver, final WebElement wrappedElement) {
        try {
            return elementClass.getDeclaredConstructor(WebDriver.class, WebElement.class).newInstance(webDriver, wrappedElement);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException
                | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}
