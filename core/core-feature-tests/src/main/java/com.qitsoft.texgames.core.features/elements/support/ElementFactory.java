package com.qitsoft.texgames.core.features.elements.support;

import com.qitsoft.texgames.core.features.elements.Element;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 25.12.13.
 */
public interface ElementFactory {

    <E extends Element> E create(Class<E> elementClass, WebDriver webDriver, WebElement wrappedElement);

}
