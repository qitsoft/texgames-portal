package com.qitsoft.texgames.core.features.elements.support;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.qitsoft.texgames.core.features.elements.Container;
import com.qitsoft.texgames.core.features.elements.Element;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

/**
 * Created by ssolovio on 25.12.13.
 */
public class ExtendedFieldDecorator extends DefaultFieldDecorator {

    private ElementFactory elementFactory = new DefaultElementFactory();

    private WebDriver webDriver;

    private SearchContext defaultContext;

    private Map<String, SearchContext> searchContexts;

    public ExtendedFieldDecorator(final WebDriver webDriver, final SearchContext defaultContext, final Map<String, SearchContext> searchContexts) {
        super(new MultiContextualElementLocatorFactory(defaultContext, searchContexts));
        this.webDriver = webDriver;
        this.defaultContext = defaultContext;
        this.searchContexts = searchContexts;
    }

    public ExtendedFieldDecorator(final WebDriver webDriver, final SearchContext defaultContext) {
        this(webDriver, defaultContext, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object decorate(final ClassLoader loader, final Field field) {
        if (!field.isAnnotationPresent(FindBy.class)
                && !field.isAnnotationPresent(FindBys.class)
                && !field.isAnnotationPresent(FindAll.class)) {
            return null;
        }

        if (Container.class.isAssignableFrom(field.getType())) {
            return decorateContainer(loader, field);
        } else if (Element.class.isAssignableFrom(field.getType())) {
            return decorateElement(loader, field);
        } else if (List.class.isAssignableFrom(field.getType())) {
            Class<?> itemType  = getListItemType(field);
            if (Element.class.isAssignableFrom(itemType)) {
                return decorateList(loader, field, (Class<? extends Element>) itemType);
            } else {
                return super.decorate(loader, field);
            }
        } else {
            return super.decorate(loader, field);
        }
    }

    @SuppressWarnings("unchecked")
    private Object decorateContainer(ClassLoader loader, Field field) {
        final WebElement wrappedElement = proxyForLocator(loader, factory.createLocator(field));
        Container container = elementFactory.create((Class<? extends Container>) field.getType(), webDriver, wrappedElement);
        PageFactory.initElements(new ExtendedFieldDecorator(webDriver, wrappedElement), container);

        return container;
    }

    @SuppressWarnings("unchecked")
    private Object decorateElement(final ClassLoader loader, final Field field) {
        final WebElement wrappedElement = proxyForLocator(loader, factory.createLocator(field));
        return elementFactory.create((Class<? extends Element>) field.getType(), webDriver, wrappedElement);
    }

    private Class<?> getListItemType(Field field) {
        Class<?> itemType = Object.class;
        if (field.getGenericType() instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
            if (parameterizedType.getActualTypeArguments().length > 0) {
                itemType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
            }
        }
        return itemType;
    }

    private Object decorateList(ClassLoader loader, Field field, final Class<? extends Element> itemType) {
        final List<WebElement> wrappedElements = proxyForListLocator(loader, factory.createLocator(field));

        return Lists.transform(wrappedElements, new Function<WebElement, Object>() {
            @Override
            public Object apply(WebElement input) {
                Object result = elementFactory.create(itemType, webDriver, input);

                if (Container.class.isAssignableFrom(itemType)) {
                    PageFactory.initElements(new ExtendedFieldDecorator(webDriver, input), result);
                }

                return result;
            }
        });
    }

}
