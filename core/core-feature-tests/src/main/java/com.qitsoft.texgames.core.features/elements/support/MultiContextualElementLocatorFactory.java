package com.qitsoft.texgames.core.features.elements.support;

import com.qitsoft.texgames.core.features.elements.WebContext;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
* Created by ssolovio on 26.12.13.
*/
class MultiContextualElementLocatorFactory implements ElementLocatorFactory {

    private ElementLocatorFactory defaultElementLocatorFactory;

    private Map<String, ElementLocatorFactory> elementLocatorFactories;

    public MultiContextualElementLocatorFactory(SearchContext defaultContext, Map<String, SearchContext> searchContexts) {
        this.elementLocatorFactories = new HashMap<>();
        if (searchContexts != null) {
            for (Map.Entry<String, SearchContext> entry : searchContexts.entrySet()) {
                this.elementLocatorFactories.put(entry.getKey(), createElementLocatorFactory(entry.getValue()));
            }
        }
        this.defaultElementLocatorFactory = createElementLocatorFactory(defaultContext);
    }

    @Override
    public ElementLocator createLocator(Field field) {
        WebContext webContext = field.getAnnotation(WebContext.class);
        if (webContext != null) {
            ElementLocatorFactory elementLocatorFactory = elementLocatorFactories.get(webContext.value());
            if (elementLocatorFactory != null) {
                return elementLocatorFactory.createLocator(field);
            }
        }

        return defaultElementLocatorFactory.createLocator(field);
    }

    private ElementLocatorFactory createElementLocatorFactory(SearchContext context) {
        return new DefaultElementLocatorFactory(context);
    }
}
