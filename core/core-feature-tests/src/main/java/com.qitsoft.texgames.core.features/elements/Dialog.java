package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by ssolovio on 03.01.14.
 */
public class Dialog extends AbstractContainer {

    @FindBy(css = ".modal-title")
    private WebElement titleElement;

    @FindBy(css = "div.modal-body")
    private WebElement bodyElement;

    @FindBy(css = "button.btn[type='button']")
    private List<Button> buttons;

    public Dialog(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public WebElement getBodyElement() {
        return bodyElement;
    }

    public WebElement getTitleElement() {
        return titleElement;
    }

    public String getTitle() {
        return titleElement.getText().trim();
    }

    public String getMessage() {
        return bodyElement.getText().trim();
    }

    public List<Button> getButtons() {
        return buttons;
    }
}
