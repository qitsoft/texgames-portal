package com.qitsoft.texgames.core.features.elements;

import cucumber.deps.difflib.StringUtills;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 14.01.14.
 */
public class Image extends AbstractElement {

    private String src;

    private int width = -1;

    private int height = -1;

    public Image(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);

    }

    public String getSrc() {
        if (src == null) {
            src = getWrappedElement().getAttribute("src");
            if (src != null) {
                src = StringUtils.substringAfterLast(src, "/").replaceFirst("\\.jpeg$", "");
            }
        }
        return src;
    }

    public int getWidth() {
        if (width < 0) {
            try {
                width = Integer.parseInt(getWrappedElement().getAttribute("width"));
            } catch (NumberFormatException e) {
                width = 0;
            }
        }
        return width;
    }

    public int getHeight() {
        if (height < 0) {
            try {
                height = Integer.parseInt(getWrappedElement().getAttribute("height"));
            } catch (NumberFormatException e) {
                height =0;
            }
        }
        return height;
    }
}
