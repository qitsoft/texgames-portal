package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 25.12.13.
 */
public interface Element {

    WebElement getWrappedElement();

    WebDriver getWebDriver();

    String getId();

    boolean isVisible();

    boolean isEnabled();

}
