package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 14.01.14.
 */
public class BaseTextualElement extends AbstractElement implements TextualElement {

    public BaseTextualElement(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    @Override
    public String getText() {
        return getWrappedElement().getText();
    }
}
