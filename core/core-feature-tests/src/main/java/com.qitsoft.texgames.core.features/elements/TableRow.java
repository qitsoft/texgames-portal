package com.qitsoft.texgames.core.features.elements;

import com.google.common.collect.Lists;
import com.qitsoft.texgames.core.features.utils.CukeFunctions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by ssolovio on 29.12.13.
 */
public class TableRow extends AbstractContainer {

    @FindBy(tagName = "td")
    private List<WebElement> cellElements;

    public TableRow(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public List<WebElement> getCellElements() {
        return cellElements;
    }

    public List<String> getTextCells() {
        return Lists.transform(cellElements, CukeFunctions.elementToTextTransformer);
    }
}
