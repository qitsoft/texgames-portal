package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 25.12.13.
 */
public class Link extends AbstractElement implements TextualElement, ClickableElement {

    public Link(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public String getHref() {
        return getWrappedElement().getAttribute("href").trim();
    }

    @Override
    public void click() {
        getWrappedElement().click();
    }

    @Override
    public String getText() {
        return getWrappedElement().getText();
    }
}
