package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by ssolovio on 25.12.13.
 */
public class Menu extends AbstractElement {

    public Menu(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public void navigateTo(String path) {
        visit(path, new MenuItemAction() {
            @Override
            public void execute(WebElement item) {
                item.findElement(By.tagName("a")).click();
            }
        });
    }

    public void visit(String path, MenuItemAction action) {
        if (path == null || path.isEmpty()) {
            return;
        }

        StringTokenizer tokenizer = new StringTokenizer(path, "/");
        WebElement element = getWrappedElement();
        while (tokenizer.hasMoreTokens()) {
            String itemText = tokenizer.nextToken();
            List<WebElement> items = element.findElements(By.xpath("ul/li"));
            if (items == null) {
                return;
            }

            for (WebElement item : items) {
                if (item.findElement(By.tagName("a")).getText().trim().equalsIgnoreCase(itemText)) {
                    action.execute(item);
                    element = item;
                    break;
                }
            }
        }
    }

    private interface MenuItemAction {

        void execute(WebElement item);

    }

}
