package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 09.01.14.
 */
public class Heading extends AbstractElement implements TextualElement {

    public Heading(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    @Override
    public String getText() {
        return getWrappedElement().getText();
    }

    public int getLevel() {
        return Integer.parseInt(getWrappedElement().getTagName().replaceFirst("[Hh]", ""));
    }
}
