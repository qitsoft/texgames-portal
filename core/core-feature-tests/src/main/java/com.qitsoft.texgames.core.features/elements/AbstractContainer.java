package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 25.12.13.
 */
public abstract class AbstractContainer extends AbstractElement implements Container {

    public AbstractContainer(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }
}
