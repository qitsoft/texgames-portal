package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 14.01.14.
 */
public class Thumbnail extends Image {

    public Thumbnail(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    @Override
    public String getSrc() {
        return super.getSrc().replaceFirst("-thumb$", "");
    }
}
