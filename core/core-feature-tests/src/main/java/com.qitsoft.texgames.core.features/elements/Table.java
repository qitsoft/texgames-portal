package com.qitsoft.texgames.core.features.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by ssolovio on 25.12.13.
 */
public class Table extends AbstractContainer {

    @FindBy(xpath = "//thead/tr")
    private TableHeader header;

    @FindBy(xpath = "//tbody/tr")
    private List<TableRow> rows;

    public Table(WebDriver webDriver, WebElement wrappedElement) {
        super(webDriver, wrappedElement);
    }

    public TableHeader getHeader() {
        return header;
    }

    public List<TableRow> getRows() {
        return rows;
    }
}
