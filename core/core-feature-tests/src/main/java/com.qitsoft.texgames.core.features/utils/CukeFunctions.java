package com.qitsoft.texgames.core.features.utils;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebElement;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * Created by ssolovio on 13.01.14.
 */
public class CukeFunctions {

    public static final Function<WebElement, String> elementToTextTransformer = new Function<WebElement, String>() {

        @Override
        public String apply(WebElement input) {
            return input.getText().trim();
        }
    };

    public static final Function<String, String> removeQuotesTransformer = new Function<String, String>() {

        @Override
        public String apply(String input) {
            return input.replaceFirst("^\\s*[\"']", "").replaceFirst("[\"']\\s*$", "");
        }
    };

    public static final Function<String, String> trimTransformer = new Function<String, String>() {

        @Override
        public String apply(String input) {
            return StringUtils.trim(input);
        }
    };

    public static final Function<String, Integer> indexOfFunction(final List<String> texts) {
        return indexOfFunction(texts, true);
    }

    public static final Function<String, Integer> indexOfFunction(final List<String> texts, final boolean assertMissing) {
        final List<String> trimmedTexts = Lists.transform(texts, CukeFunctions.trimTransformer);
        return new Function<String, Integer>() {

            @Override
            public Integer apply(String input) {
                int result = trimmedTexts.indexOf(StringUtils.trim(input));
                if (assertMissing && result < 0) {
                    fail(String.format("Unable to find element \"%s\" in the list %s", input, Iterables.toString(texts)));
                }
                return result;
            }
        };
    }

    public static final <F, T> Function<F, T> union(final Collection<Function<F, T>> functions, final Predicate<T> skipPredicate) {
        Assert.notNull(functions);
        Assert.notNull(skipPredicate);

        final Collection<Function<F, T>> nonNullFunctions = Collections2.filter(functions, Predicates.notNull());
        Assert.notEmpty(nonNullFunctions);

        return new Function<F, T>() {
            @Override
            public T apply(F input) {
                Iterator<Function<F, T>> iterator = nonNullFunctions.iterator();

                T result;
                do {
                    Function<F, T> function = iterator.next();
                    result = function.apply(input);
                } while (iterator.hasNext() && skipPredicate.apply(result));

                return result;
            }
        };
    }
}
