package com.qitsoft.texgames.core.features.utils;

import com.qitsoft.texgames.core.features.elements.Button;
import com.qitsoft.texgames.core.features.elements.TableRow;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 13.01.14.
 */
public class CukeMatchers {
    public static Matcher<? super TableRow> matchesRowWithItem(final String cellItemText) {
        return new BaseMatcher<TableRow>() {

            @Override
            public boolean matches(Object item) {
                TableRow row = (TableRow) item;
                return row.getTextCells().contains(cellItemText);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("a row with item [").appendText(cellItemText).appendText("].");
            }
        };
    }

    public static Matcher<? super Button> matchesButtonName(final String buttonName) {
        return new BaseMatcher<Button>() {

            @Override
            public boolean matches(Object item) {
                Button button = (Button) item;
                return buttonName.equals(button.getText());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("the button with name \"").appendText(buttonName).appendText("\".");
            }
        };
    }

    public static Matcher<WebElement> matchesVisibleElement() {
        return new BaseMatcher<WebElement>() {
            @Override
            public boolean matches(Object item) {
                WebElement element = (WebElement) item;
                return element.isDisplayed();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("visible element.");
            }
        };
    }
}
