package com.qitsoft.texgames.core.features.utils;

import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.Locale;

/**
 * Created by ssolovio on 14.01.14.
 */
public class FirefoxProfileBuilder {

    private String preferredLocale;

    public void setPreferredLocale(String locale) {
        preferredLocale = locale;
    }

    public FirefoxProfile build() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("intl.accept_languages", preferredLocale);
        return profile;
    }

}
