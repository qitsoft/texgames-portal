package com.qitsoft.texgames.core.features.utils;

import com.google.common.base.Predicate;
import com.qitsoft.texgames.core.features.elements.Button;
import com.qitsoft.texgames.core.features.elements.Dialog;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebElement;

/**
 * Created by ssolovio on 13.01.14.
 */
public class CukePredicates {
    public static final Predicate<WebElement> isDisabled = new Predicate<WebElement>() {
        @Override
        public boolean apply(WebElement input) {
            return input.getAttribute("class").contains("disabled");
        }
    };
    public static final Predicate<WebElement> isEnabled = new Predicate<WebElement>() {
        @Override
        public boolean apply(WebElement input) {
            return !input.getAttribute("class").contains("disabled");
        }
    };
    public static final Predicate<WebElement> isElementVisible = new Predicate<WebElement>() {
        @Override
        public boolean apply(WebElement input) {
            return input.isDisplayed();
        }
    };
    public static final Predicate<String> significantTitlePredicate = new Predicate<String>() {

        @Override
        public boolean apply(String input) {
            return StringUtils.isNotBlank(input) && !"#".equals(input);
        }
    };
    static final Predicate<? super Dialog> PREDICATE_IS_VISIBLE = new Predicate<Dialog>() {
        @Override
        public boolean apply(Dialog input) {
            return input.isVisible();
        }
    };

    public static Predicate<? super Dialog> isVisible() {
        return CukePredicates.PREDICATE_IS_VISIBLE;
    }

    public static final Predicate<WebElement> isButton(final String name) {
        return new Predicate<WebElement>() {
            @Override
            public boolean apply(WebElement input) {
                return name.equalsIgnoreCase(input.getText().trim()) || StringUtils.endsWithIgnoreCase(input.getAttribute("title"), name);
            }
        };
    }

    public static final Predicate<Integer> lessThan(final int value) {
        return new Predicate<Integer>() {
            @Override
            public boolean apply(Integer input) {
                return input < value;
            }
        };
    }

    public static Predicate<? super Button> buttonName(final String buttonName) {
        return new Predicate<Button>() {
            @Override
            public boolean apply(Button input) {
                return buttonName.equals(input.getText().trim());
            }
        };
    }
}
