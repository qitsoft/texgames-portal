package com.qitsoft.texgames.core.features.stepdefs;

import com.qitsoft.texgames.core.features.stepdefs.BaseStepdefs;
import com.qitsoft.texgames.core.features.pages.Page;
import cucumber.api.java.ru.Пусть;

/**
 * Created by ssolovio on 22.12.13.
 */
public class MenuStepdefs extends BaseStepdefs {

    @Пусть("^мы нажали в меню на \"([^\"]*)\"$")
    public void мы_нажали_в_меню_на(String menuPath) throws Throwable {
        Page page = createPage(Page.class);
        page.getMenu().navigateTo(menuPath);
    }

}
