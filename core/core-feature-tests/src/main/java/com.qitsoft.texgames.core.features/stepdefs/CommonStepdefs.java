package com.qitsoft.texgames.core.features.stepdefs;

import com.google.common.collect.Collections2;
import com.qitsoft.texgames.core.features.stepdefs.BaseStepdefs;
import com.qitsoft.texgames.core.features.elements.Button;
import com.qitsoft.texgames.core.features.elements.Dialog;
import com.qitsoft.texgames.core.features.pages.Page;
import com.qitsoft.texgames.core.features.utils.CukeMatchers;
import com.qitsoft.texgames.core.features.utils.CukePredicates;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Пусть;
import cucumber.api.java.ru.Тогда;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.openqa.selenium.NoSuchElementException;

import java.util.Iterator;
import java.util.List;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.*;

/**
 * Created by ssolovio on 03.01.14.
 */
public class CommonStepdefs extends BaseStepdefs {

    @Тогда("^должна открыться страница \"(.*)\"$")
    public void должна_открыться_страница(String name) throws Throwable {
        Page page = createPage(Page.class);
        name = name.toLowerCase();
        String title = page.getTitle().toLowerCase();
        String heading = "";
        try {
            heading = page.getH1().getText().toLowerCase();
        } catch (NoSuchElementException e) {
        }
        if (!StringUtils.containsIgnoreCase(title, name) && !StringUtils.containsIgnoreCase(heading, name)) {
            fail(String.format("Wanted [%s] but was [%s].", name, title));
        }
    }

    @Тогда("^должно открыться всплавающее окно с сообщением \"(.*\\?)\" и кнопками \"([^\"]*)\" и \"([^\"]*)\".$")
    public void должно_открыться_всплавающее_окно_с_сообщением_и_кнопками(String message, String button1, String button2) throws Throwable {
        Page page = createPage(Page.class);
        Dialog dialog = page.waitOpeningDialog();
        assertNotNull(dialog);
        assertEquals(message, dialog.getMessage());
        List<Button> buttons = dialog.getButtons();
        assertEquals(2, buttons.size());
        assertThat(buttons, hasItem(CukeMatchers.matchesButtonName(button1)));
        assertThat(buttons, hasItem(CukeMatchers.matchesButtonName(button2)));
    }

    @Если("^в всплывающем окне нажать на кнопку \"([^\"]*)\"$")
    public void в_сплывающем_окне_нажать_на_кнопку(String buttonName) throws Throwable {
        Page page = createPage(Page.class);
        Dialog dialog = page.waitOpeningDialog();

        List<Button> buttons = dialog.getButtons();
        Iterator<Button> itarator = Collections2.filter(buttons, CukePredicates.buttonName(buttonName)).iterator();
        assertTrue("There is no button with name [" + buttonName + "].", itarator.hasNext());

        itarator.next().click();
    }

    @Тогда("^всплывающее окно закрылось$")
    public void сплывающее_окно_закрылось() {
        Page page = createPage(Page.class);
        assertTrue(page.waitClosingDialog());
    }

    @Пусть("^мы нажали на кнопку \"([^\"]*)\"$")
    public void мы_нажали_на_кнопку(String buttonName) throws Throwable {
        Page page = createPage(Page.class);
        Button button = page.findContentButton(buttonName);
        button.click();
    }

    @Тогда("^должно быть сообщение \"(.*)\"$")
    public void должно_быть_сообщение(String message) throws Throwable {
        Page page = createPage(Page.class);
        assertNotNull(page.getTopMessage());
        assertThat(page.getTopMessage(), Matchers.equalToIgnoringCase(message));
    }
//
//    @Тогда("^кнопка \"([^\"]*)\" должна быть не активна$")
//    public void кнопка_должна_быть_не_активна(String buttonName) throws Throwable {
//        Page page = createPage(Page.class);
//
//        Button button = page.findContentButton(buttonName);
//        assertFalse(button.isEnabled());
//    }
//
//    @Тогда("^кнопка \"([^\"]*)\" должна стать активной$")
//    public void кнопка_должна_стать_активной(String buttonName) throws Throwable {
//        Page page = createPage(Page.class);
//
//        Button button = page.findContentButton(buttonName);
//        assertTrue(button.isEnabled());
//    }
}
