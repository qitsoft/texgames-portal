package com.qitsoft.texgames.core.features.stepdefs;

import com.google.common.collect.Maps;
import com.mysema.query.jpa.hibernate.HibernateQuery;
import com.qitsoft.texgames.core.features.DatabaseManager;
import com.qitsoft.texgames.core.features.pages.TablePage;
import com.qitsoft.texgames.core.features.utils.CukeFunctions;
import com.qitsoft.texgames.core.features.utils.CukePredicates;
import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.model.QGame;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.ru.Пусть;
import cucumber.api.java.ru.Тогда;
import liquibase.exception.LiquibaseException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by ssolovio on 28.12.13.
 */
@ContextConfiguration(locations = "classpath:featureTestsContext.xml")
public class DatabaseStepdef extends BaseStepdefs {

    @Autowired
    private DatabaseManager databaseManager;

    @Before
    public void setUp() throws SQLException, LiquibaseException {
        databaseManager.setUpDatabase();
        databaseManager.openSession();
    }

    @After
    public void tearDown() {
        databaseManager.closeSession();
    }

    @SuppressWarnings("unchecked")
    @Пусть("^в системе есть (.*):$")
    public void в_системе_есть(String entityLocalName, DataTable values) throws Throwable {
        Session session = databaseManager.getSession();

        for (Map<String, String> map : values.asMaps()) {
            Class entityClass = databaseManager.getEntityClass(entityLocalName);
            ClassMetadata entityClassMetadata = databaseManager.getSessionFactory().getClassMetadata(entityClass);

            Object entityObject = entityClass.newInstance();

            for (Map.Entry<String, String> entry : map.entrySet()) {
                String propertyName = databaseManager.getEntities().getProperty(entry.getKey().trim().toLowerCase());
                Class propertyClass = entityClassMetadata.getPropertyType(propertyName).getReturnedClass();

                if (Integer.class.isAssignableFrom(propertyClass)) {
                    entityClassMetadata.setPropertyValue(entityObject, propertyName, Integer.parseInt(entry.getValue()));
                } else if (Boolean.class.isAssignableFrom(propertyClass)) {
                    entityClassMetadata.setPropertyValue(entityObject, propertyName, Boolean.parseBoolean(entry.getValue()));
                } else {
                    entityClassMetadata.setPropertyValue(entityObject, propertyName, entry.getValue());
                }
            }
            session.persist(entityObject);
            session.flush();
        }
    }

    @Пусть("^в системе есть более (\\d+) (.*)$")
    public void в_системе_есть_более_элементов(int num, String entityLocalName) throws Throwable {
        Session session = databaseManager.getSession();
        Class entityClass = databaseManager.getEntityClass(entityLocalName);
        ClassMetadata entityClassMetadata = databaseManager.getSessionFactory().getClassMetadata(entityClass);

        for (int i = 0; i <= num; i++) {
            Object entityObject = entityClass.newInstance();

            for(String propertyName : entityClassMetadata.getPropertyNames()) {
                Class propertyClass = entityClassMetadata.getPropertyType(propertyName).getReturnedClass();
                if (Integer.class.isAssignableFrom(propertyClass)) {
                    entityClassMetadata.setPropertyValue(entityObject, propertyName, i+10000);
                } else if (Boolean.class.isAssignableFrom(propertyClass)) {
                    entityClassMetadata.setPropertyValue(entityObject, propertyName, i % 2);
                } else if (String.class.isAssignableFrom(propertyClass)) {
                    entityClassMetadata.setPropertyValue(entityObject, propertyName, String.format("%s_%d", propertyName, i));
                }
            }
            session.persist(entityObject);
            session.flush();
        }
    }


    @Тогда("^первый элемент в таблице указывает на игру с порядковым номером (\\d+) после сортировки$")
    public void первый_элемент_в_таблице_указывает_на_игру_с_порядковым_номером_после_сортировки(int position) throws Throwable {
        Session session = databaseManager.getSession();

        Game game = new HibernateQuery(session)
                .from(QGame.game)
                .orderBy(QGame.game.plays.desc())
                .limit(1)
                .offset(position - 1)
                .uniqueResult(QGame.game);
        assertNotNull(game);

        TablePage page = createPage(TablePage.class);
        assertFalse(page.getTable().getRows().isEmpty());

        Map<String, Integer> headerMap = Maps.toMap(
                page.getTable().getHeader().getTextCells(),
                CukeFunctions.indexOfFunction(page.getTable().getHeader().getTextCells()));

        headerMap = Maps.filterKeys(headerMap, CukePredicates.significantTitlePredicate);
        ClassMetadata entityClassMetadata = databaseManager.getSessionFactory().getClassMetadata(Game.class);
        List<String> tableCells = page.getTable().getRows().get(0).getTextCells();
        for(Map.Entry<String, Integer> entry : headerMap.entrySet()) {
            String propertyName = databaseManager.getEntities().getProperty(entry.getKey().trim().toLowerCase());
            if (propertyName == null) {
                continue;
            }

            Type propertyType = entityClassMetadata.getPropertyType(propertyName);
            if (propertyType == null) {
                continue;
            }
            Class propertyClass = propertyType.getReturnedClass();

            if (Integer.class.isAssignableFrom(propertyClass)) {
                assertEquals(Integer.parseInt(tableCells.get(entry.getValue())), entityClassMetadata.getPropertyValue(game, propertyName));
            } else if (Boolean.class.isAssignableFrom(propertyClass)) {
                assertEquals(Boolean.parseBoolean(tableCells.get(entry.getValue())), entityClassMetadata.getPropertyValue(game, propertyName));
            } else if (String.class.isAssignableFrom(propertyClass)) {
                assertEquals(tableCells.get(entry.getValue()), entityClassMetadata.getPropertyValue(game, propertyName));
            }
        }
    }


}
