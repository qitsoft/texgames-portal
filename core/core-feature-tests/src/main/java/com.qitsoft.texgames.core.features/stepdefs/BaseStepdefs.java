package com.qitsoft.texgames.core.features.stepdefs;

import com.qitsoft.texgames.core.features.pages.PageFactory;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.logging.Logger;

/**
 * Created by ssolovio on 25.12.13.
 */
@ContextConfiguration(locations = "classpath:featureTestsContext.xml")
public abstract class BaseStepdefs {

    protected final Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    private WebDriver webDriver;

    @Autowired
    private PageFactory pageFactory;

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public <T> T createPage(Class<? extends T> pageClass) {
        return pageFactory.createPage(pageClass);
    }
}
