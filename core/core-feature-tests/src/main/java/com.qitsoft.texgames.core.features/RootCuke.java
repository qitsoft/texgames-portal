package com.qitsoft.texgames.core.features;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by ssolovio on 22.12.13.
 */
@ContextConfiguration(locations = "classpath:featureTestsContext.xml")
public class RootCuke {

    @Autowired
    private WebDriver webDriver;

    @Autowired
    private CukeConfig config;

    @Before
    public void startBrowser() throws IOException {
        webDriver.get("http://localhost:8080" + config.getContextPath());
    }

    @After
    public void closeBrowser() {
        webDriver.close();
    }

}
