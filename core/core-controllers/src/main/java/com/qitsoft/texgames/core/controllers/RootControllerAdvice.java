package com.qitsoft.texgames.core.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ssolovio on 30.12.13.
 */
@ControllerAdvice
public class RootControllerAdvice {

    @ModelAttribute("baseUrl")
    public String baseUrl(HttpServletRequest request) {
        return request.getContextPath();
    }

    @ModelAttribute("imageBaseUrl")
    public String imageBaseUrl(HttpServletRequest request) {
        return request.getContextPath() + "/images";
    }

}
