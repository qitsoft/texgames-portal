package com.qitsoft.texgames.core.controllers;

import com.qitsoft.texgames.core.config.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;

/**
 * Created by ssolovio on 12.01.14.
 */
@Controller
@RequestMapping("/images")
public class ImagesController {

    @RequestMapping(value = "/tmp/{imageName}", method = RequestMethod.GET, produces = "image/jpeg")
    @ResponseBody
    public byte[] tmp(@PathVariable String imageName) throws IOException {
        return readImage(Constants.Directories.TEMP_DIR, imageName);
    }

    @RequestMapping(value = "/{imageName}", method = RequestMethod.GET, produces = "image/jpeg")
    @ResponseBody
    public byte[] image(@PathVariable String imageName) throws IOException {
        return readImage(Constants.Directories.IMAGES_DIR, imageName);
    }

    private byte[] readImage(File dir, String imageName) throws IOException {
        imageName = imageName + ".jpeg";
        File imageFile = new File(dir, imageName);
        try {
            return Files.readAllBytes(imageFile.toPath());
        } catch (NoSuchFileException e) {
            throw new FileNotFoundException(e.getLocalizedMessage());
        }
    }
}
