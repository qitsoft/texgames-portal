package com.qitsoft.texgames.core.controllers;

import com.qitsoft.texgames.core.config.Constants;

/**
 * Created by ssolovio on 03.01.14.
 */
public final class Utils {

    private Utils() {
    }

    public static int getPageByPosition(int pos) {
        return ((pos - 1) / Constants.PAGE_SIZE) + 1;
    }

}
