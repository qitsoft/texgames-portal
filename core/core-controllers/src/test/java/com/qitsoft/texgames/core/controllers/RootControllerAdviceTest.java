package com.qitsoft.texgames.core.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by ssolovio on 15.01.14.
 */
@RunWith(MockitoJUnitRunner.class)
public class RootControllerAdviceTest {

    private static final String CONTEXT_PATH = "/theWebContext";

    private RootControllerAdvice controllerAdvice = new RootControllerAdvice();

    @Mock
    private HttpServletRequest request;

    @Before
    public void setUp() {
        when(request.getContextPath()).thenReturn(CONTEXT_PATH);
    }

    @Test
    public void testBaseUrl() {
        assertEquals(CONTEXT_PATH, controllerAdvice.baseUrl(request));
    }

    @Test
    public void testImagesBaseUrl() {
        assertEquals(CONTEXT_PATH + "/images", controllerAdvice.imageBaseUrl(request));
    }

}
