package com.qitsoft.texgames.core.controllers;

import com.google.common.io.Files;
import com.qitsoft.texgames.core.config.Constants;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

/**
 * Created by ssolovio on 12.01.14.
 */
public class ImagesControllerTest {

    private ImagesController controller = new ImagesController();

    @Before
    public void setUp() throws IOException {
        FileUtils.cleanDirectory(Constants.Directories.TEMP_DIR);
        File[] files = Constants.Directories.IMAGES_DIR.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("testImageFile");
            }
        });
        for (File file : files) {
            file.delete();
        }
    }

    @Test
    public void testTempReturnImage() throws IOException {
        String imageFileContent = "Simple image file content";
        File image = new File(Constants.Directories.TEMP_DIR, "testImageFile.jpeg");
        Files.write(imageFileContent, image, Charset.forName("UTF8"));

        byte[] data = controller.tmp("testImageFile");
        assertEquals(imageFileContent, new String(data, Charset.forName("UTF8")));
    }

    @Test
    public void testTempReturnOnlyJpeg() throws IOException {
        String pngImageFileContent = "Simple PNG image file content";
        String jpegImageFileContent = "Simple JPEG image file content";

        File image = new File(Constants.Directories.TEMP_DIR, "testImageFile.png");
        Files.write(pngImageFileContent, image, Charset.forName("UTF8"));

        image = new File(Constants.Directories.TEMP_DIR, "testImageFile.jpeg");
        Files.write(jpegImageFileContent, image, Charset.forName("UTF8"));

        byte[] data = controller.tmp("testImageFile");
        assertEquals(jpegImageFileContent, new String(data, Charset.forName("UTF8")));
    }

    @Test(expected = FileNotFoundException.class)
    public void testTemp404() throws IOException {
        controller.tmp("testImageFile");
    }

    @Test
    public void testReturnImage() throws IOException {
        String imageFileContent = "Simple image file content";
        File image = new File(Constants.Directories.IMAGES_DIR, "testImageFile.jpeg");
        Files.write(imageFileContent, image, Charset.forName("UTF8"));

        byte[] data = controller.image("testImageFile");
        assertEquals(imageFileContent, new String(data, Charset.forName("UTF8")));
    }

    @Test
    public void testReturnOnlyJpeg() throws IOException {
        String pngImageFileContent = "Simple PNG image file content";
        String jpegImageFileContent = "Simple JPEG image file content";

        File image = new File(Constants.Directories.IMAGES_DIR, "testImageFile.png");
        Files.write(pngImageFileContent, image, Charset.forName("UTF8"));

        image = new File(Constants.Directories.IMAGES_DIR, "testImageFile.jpeg");
        Files.write(jpegImageFileContent, image, Charset.forName("UTF8"));

        byte[] data = controller.image("testImageFile");
        assertEquals(jpegImageFileContent, new String(data, Charset.forName("UTF8")));
    }

    @Test(expected = FileNotFoundException.class)
    public void test404() throws IOException {
        controller.image("testImageFile");
    }
}
