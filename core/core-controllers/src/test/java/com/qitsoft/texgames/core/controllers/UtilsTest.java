package com.qitsoft.texgames.core.controllers;

import com.qitsoft.texgames.core.config.Constants;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ssolovio on 03.01.14.
 */
public class UtilsTest {

    @Test
    public void getPageOfFirstItem() {
        assertEquals(1, Utils.getPageByPosition(1));
    }

    @Test
    public void getPageOfLastItemOnTheFirstPage() {
        assertEquals(1, Utils.getPageByPosition(Constants.PAGE_SIZE));
    }

    @Test
    public void getPageOfFirstItemOnTheSecondPage() {
        assertEquals(2, Utils.getPageByPosition(Constants.PAGE_SIZE + 1));
    }

    @Test
    public void getPageOfLastItemOnTheSecondPage() {
        assertEquals(2, Utils.getPageByPosition(2 * Constants.PAGE_SIZE));
    }
}
