<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="d" tagdir="/WEB-INF/tags/dialogs" %>
<c:choose>
    <c:when test="${game.id != 0}">
        <c:set var="pageTitle" value="Редактирование игры \"${game.name}\"" />
    </c:when>
    <c:otherwise>
        <c:set var="pageTitle" value="Добавление игры" />
    </c:otherwise>
</c:choose>
<t:main title="${pageTitle}" heading="${pageTitle}">
    <br/>
    <form:form role="form" cssClass="form-horizontal" novalidate="true" modelAttribute="game" action="${baseUrl}/games/save">
        <form:hidden path="id" />
        <div class="form-group">
            <label for="image" class="control-label col-sm-2 required">Картинка</label>
            <div class="file-upload col-sm-10">
                <img id="image-image" class="thumb"
                     <c:choose>
                         <c:when test="${game.id != 0}">
                             src="${imageBaseUrl}/${game.image}-thumb.jpeg"
                             width="100"
                             height="100"
                         </c:when>
                         <c:otherwise>
                             style="display: none;"
                         </c:otherwise>
                     </c:choose>
                />
                <span id="btn-image" class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    Загрузить
                    <input id="image-file" name="image-file" type="file" accept="image/jpeg" data-url="${baseUrl}/games/upload-image" />
                </span>
                <div id="progressbar-image" class="progress progress-striped" >
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                        <span class="sr-only">0% Выполнено</span>
                    </div>
                </div>
                <form:hidden id="image" path="image"
                             required="true" data-validation-required-message="Загрузите картинку для игры"
                             data-validation-callback-callback="has_image_upload_errors" />
                <p class="help-block"></p>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label col-sm-2 required">Название</label>
            <div class="col-sm-10">
                <form:input id="name" path="name" cssClass="form-control" placeholder="Название" autofocus="true"
                            required="true" data-validation-required-message="Введите название игры" />
                <p class="help-block"></p>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="control-label col-sm-2 required">Описание</label>
            <div class="col-sm-10">
                <form:textarea id="description" path="description" cssClass="wysiwyg form-control" rows="10" required="true" data-validation-required-message="Введите описание игры" />
                <p class="help-block"></p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <c:choose>
                    <c:when test="${game.id != 0}">
                        <form:button name="submit" class="btn btn-success" title="Сохранить игру" >Сохранить игру</form:button>
                    </c:when>
                    <c:otherwise>
                        <form:button name="submit" class="btn btn-success" title="Добавить игру" >Добавить игру</form:button>
                    </c:otherwise>
                </c:choose>
                &nbsp;&nbsp;&nbsp;
                <button name="cancel" class="btn" title="Отмена" onclick="call('games');return false;" >Отмена</button>
            </div>
        </div>
    </form:form>
</t:main>