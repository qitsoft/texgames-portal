<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="d" tagdir="/WEB-INF/tags/dialogs" %>
<t:main title="Игры" heading="Игры">
    <c:choose>
        <c:when test="${not empty games}">
            <d:delete title="Удаление игры">
                Вы уверены что хотите удалить игру "<span id="deleteGameName"></span>"?
            </d:delete>

            <button class="btn btn-success" title="Добавить игру" onclick="call('games/add')">Добавить игру</button>
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Название</th>
                        <th>Описание</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="game" items="${games.items}" varStatus="gameStatus">
                        <tr>
                            <td><img src="${baseUrl}/images/${game.image}-thumb.jpeg" height="100" width="100"/></td>
                            <td>${game.name}</td>
                            <td>${game.description}</td>
                            <td>
                                <button type="button" class="btn btn-primary" title="Править" onclick="call('games/edit-${game.id}')">Править</button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="button" class="btn btn-danger" title="Удалить" onclick="showDialog('deleteDialog', {deleteGameName:'${game.name}', ok: 'games/delete-${game.id}'});">Удалить</button>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <t:paginator data="${games}" pagePath="games" />
         </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
</t:main>