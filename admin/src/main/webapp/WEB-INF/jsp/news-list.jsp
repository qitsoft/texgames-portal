<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="d" tagdir="/WEB-INF/tags/dialogs" %>
<t:main title="Новости" heading="Новости">
    <%--<c:choose>--%>
        <%--<c:when test="${not empty games}">--%>
            <button class="btn btn-success" title="Добавить новость">Добавить новость</button>
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Заголовок</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <%--<c:forEach var="game" items="${games.items}" varStatus="gameStatus">--%>
                        <%--<tr>--%>
                            <%--<td><img src="${baseUrl}/images/${game.image}-thumb.jpeg" height="100" width="100"/></td>--%>
                            <%--<td>${game.name}</td>--%>
                            <%--<td>${game.description}</td>--%>
                            <%--<td>--%>
                                <%--<button type="button" class="btn btn-default <c:if test="${games.lastPage and gameStatus.last}">disabled</c:if>" title="Переместить вниз" onclick="call('games/down-${game.id}')">--%>
                                    <%--<span class="glyphicon glyphicon-arrow-down"></span>--%>
                                <%--</button>--%>
                                <%--<button type="button" class="btn btn-default <c:if test="${games.firstPage and gameStatus.first}">disabled</c:if>" title="Переместить вверх" onclick="call('games/up-${game.id}')">--%>
                                    <%--<span class="glyphicon glyphicon-arrow-up"></span>--%>
                                <%--</button>--%>
                                <%--<button type="button" class="btn btn-primary" title="Править" onclick="call('games/edit-${game.id}')">Править</button>--%>
                                <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                                <%--<button type="button" class="btn btn-danger" title="Удалить" onclick="showDialog('deleteDialog', {deleteGameName:'${game.name}', ok: 'games/delete-${game.id}'});">Удалить</button>--%>
                            <%--</td>--%>
                        <%--</tr>--%>
                    <%--</c:forEach>--%>
                </tbody>
            </table>
            <%--<t:paginator data="${games}" pagePath="games" />--%>
         <%--</c:when>--%>
        <%--<c:otherwise>--%>

        <%--</c:otherwise>--%>
    <%--</c:choose>--%>
</t:main>