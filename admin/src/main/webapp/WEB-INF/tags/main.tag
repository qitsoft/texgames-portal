<%@tag pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="p" tagdir="/WEB-INF/tags/page" %>
<%@taglib prefix="h" tagdir="/WEB-INF/tags/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="title" %>
<%@attribute name="heading" %>
<html>
    <head>
        <title>${title}</title>
        <h:header />
    </head>
    <body>
        <div class="container">
            <p:header />
            <div id="content" class="content">
                <c:if test="${not empty heading}">
                    <h1>${heading}</h1>
                </c:if>
                <c:if test="${topMessage != null}">
                    <div id="top-message" class="alert alert-${topMessage.status} alert-dismissable">${topMessage.message}</div>
                </c:if>
                <jsp:doBody />
            </div>
            <p:footer />
        </div>
        <h:scripts />
    </body>
</html>
