<%@tag pageEncoding="UTF-8" %>
<nav id="mainmenu" class="navbar navbar-default" role="navigation">
    <ul class="nav navbar-nav">
        <li><a href="${baseUrl}/">Приборная доска</a></li>
        <li id="mi_content" class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Содержимое <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a id="mi_news" href="${baseUrl}/news">Новости</a></li>
                <li><a id="mi_games" href="${baseUrl}/games">Игры</a></li>
            </ul>
        </li>
        <li><a href="#">Форум</a></li>
        <li><a href="#">Пользователи</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Выход</a></li>
    </ul>
</nav>
