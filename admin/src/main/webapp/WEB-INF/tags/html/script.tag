<%@tag pageEncoding="UTF-8" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.ArrayList" %>
<%@attribute name="address" %>
<%
    List<String> scripts = (List<String>) pageContext.getAttribute("pageScripts");
    if (scripts == null) {
        scripts = new ArrayList<>();
        pageContext.setAttribute("pageScripts", scripts);
    }
    scripts.add(address);
%>