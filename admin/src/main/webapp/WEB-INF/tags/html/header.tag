<%@tag pageEncoding="UTF-8" %>
<%@taglib prefix="h" tagdir="/WEB-INF/tags/html" %>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<h:styles />
<style>
    .header {
        height: 50px;
    }

    .navbar-right {
        padding-right: 15px;
    }

    .content {
        min-height: 200px;
    }

    .footer {
        border-top: 1px solid #eee;
        margin-top: 40px;
        padding-top: 40px;
        padding-bottom: 40px;
    }
</style>
<script type="text/javascript">
    var baseUrl = "${baseUrl}";
</script>