<%@tag pageEncoding="UTF-8" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.ArrayList" %>
<%@attribute name="address" fragment="true" %>
<%
    List<String> styles = (List<String>) pageContext.getAttribute("page-styles");
    if (styles == null) {
        styles = new ArrayList<>();
        pageContext.setAttribute("page-styles", styles);
    }
    styles.add(address);
%>