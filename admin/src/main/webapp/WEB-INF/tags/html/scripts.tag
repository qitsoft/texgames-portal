<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag import="java.util.List" %>
<%@tag pageEncoding="UTF-8" %>
<script src="${baseUrl}/ext/jquery/2.0.3/jquery.min.js"></script>
<script src="${baseUrl}/ext/bootstrap/3.0.3/js/bootstrap.js"></script>

<script src="${baseUrl}/ext/jquery-ui/1.10.3/ui/jquery.ui.widget.js"></script>

<script src="${baseUrl}/ext/tinymce-jquery/4.0.10/jquery.tinymce.min.js"></script>
<script src="${baseUrl}/ext/jqBootstrapValidation/jqBootstrapValidation-1.3.7.min.js"></script>

<script src="${baseUrl}/ext/jquery-file-upload/9.5.2/js/jquery.fileupload.js"></script>
<script src="${baseUrl}/scripts/common.js"></script>
<script type="text/javascript">
    function has_image_upload_errors($el, value, callback) {
        console.debug("CHECK");
        callback({
            value: value,
            valid: !/ERROR:\s*/.test(value),
            message: value.replace(/ERROR:\s*/, "")
        });
    }

    $(function(){
        $('textarea.wysiwyg').tinymce({
            script_url: "${baseUrl}/ext/tinymce-jquery/4.0.10/tinymce.min.js",
            plugins: [ "code link textcolor" ],
            toolbar1: "undo redo | cut copy paste | fontsizeselect forecolor " +
                    "| bold italic underline | alignleft aligncenter alignright alignjustify " +
                    "| bullist numlist outdent indent | link unlink | removeformat",
            menubar: false,
            statusbar: false,
            setup: function (ed) {
                ed.on('init', function(args) {
                    $(this.editorContainer).addClass("form-control");
                });
            }
        });

        $("input:file").fileupload({
            autoUpload: true,
            maxFileSize: 102400,
            progressall: function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var progressbar = $("#progressbar-"+this.id.replace(/-file$/, ""));
                if (progress > 0 && progress < 100) {
                    progressbar.show();
                    progressbar.css({"width":progress+"%"});
                    $("span", progress).html(progress+"% Выполнено");
                } else {
                    progressbar.hide();
                }
            },
            submit: function(e, data) {
                var formGroup = $(this).parents(".form-group");
                var fileIdField = $("input#"+this.id.replace(/-file$/, "")+":hidden");
                var helpBlock = $(".help-block", formGroup);
                var image = $("img.thumb", formGroup);
                fileIdField.val("");
                helpBlock.html("")
                image.removeAttr("src");
                image.removeAttr("width");
                image.removeAttr("height");
                image.hide();
                formGroup.removeClass("has-error");
                formGroup.removeClass("has-warning");
                formGroup.removeClass("has-success");
            },
            done: function (e, data) {
                if (!data.result.files || data.result.files.length == 0) {
                    return;
                }
                var file = data.result.files[0];
                var formGroup = $(this).parents(".form-group");
                var fileIdField = $("input#"+this.id.replace(/-file$/, "")+":hidden");
                var helpBlock = $(".help-block", formGroup);
                var image = $("img.thumb", formGroup);

                if (!file.error) {
                    formGroup.addClass("has-success");
                    fileIdField.val(file.id);
                    image.attr({
                        src: file.thumbnailUrl,
                        width: "100px",
                        height: "100px"
                    });
                    image.show();
                } else {
                    var ul = $("<ul></ul>").attr({role: "alert"});
                    var li = $("<li></li>");
                    li.addClass("image-upload-error")
                    ul.append(li);
                    li.html(file.error);
                    helpBlock.append(ul);
                    formGroup.addClass("has-error");
                    fileIdField.val("ERROR: " + file.error);
                    fileIdField.triggerHandler("validation.validation");
                }
            },
            fail: function(e, data) {
                var ul = $("<ul></ul>").attr({role: "alert"});
                var li = $("<li></li>");
                li.addClass("image-upload-error")
                ul.append(li);
                li.html(data.errorThrown);
                helpBlock.append(ul);
                formGroup.addClass("has-error");
                fileIdField.val("ERROR: " + data.errorThrown);
                fileIdField.triggerHandler("validation.validation");
            }
        })

        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation({
            classNames: {
                group: ".form-group",
                warning: "has-warning",
                error: "has-error",
                success: "has-success"
            },
            submitError: function ($form, event, errors) {
                $form.removeClass("has-error");
            }
        });

        setTimeout(function() {
            $("#top-message").fadeOut(500, function() { $().remove(this); });
        }, 5000);
    });
</script>
