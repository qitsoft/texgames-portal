function showDialog(dialogId, data) {
    if (data) {
        for(key in data) {
            if (key == "ok") {
                $("#deleteDialogOkButton").click(function() {
                    call(data[key]);
                });
            } else {
                $("#"+key).html(data[key]);
            }
        }
    }
    $("#"+dialogId).modal('show');
}

function call(method) {
    window.location = baseUrl + "/" + method;
}