package com.qitsoft.texgames.controller.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssolovio on 11.01.14.
 */
public class UploadedFiles {

    private final List<UploadedFile> files = new ArrayList<>();

    public List<UploadedFile> getFiles() {
        return files;
    }

}
