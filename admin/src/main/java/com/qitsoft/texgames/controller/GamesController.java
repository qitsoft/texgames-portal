package com.qitsoft.texgames.controller;

import com.qitsoft.texgames.controller.data.TopMessage;
import com.qitsoft.texgames.controller.data.UploadedFile;
import com.qitsoft.texgames.controller.data.UploadedFiles;
import com.qitsoft.texgames.core.config.Constants;
import com.qitsoft.texgames.core.controllers.Utils;
import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.service.GamesService;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

/**
 * Created by ssolovio on 29.12.13.
 */
@Controller
@RequestMapping("/games")
public class GamesController {

    private static final int MAX_IMAGE_SIZE = 100 * 1024;

    private static final int THUMB_WIDTH = 100;

    private static final int THUMB_HEIGHT = 100;

    @Autowired
    private GamesService gamesService;

    @RequestMapping
    public ModelAndView games() {
        return games(1);
    }

    @RequestMapping("/{page}")
    public ModelAndView games(@PathVariable int page) {
        ModelAndView modelAndView = new ModelAndView("games-list");
        modelAndView.addObject("games", gamesService.list(page, Constants.PAGE_SIZE));
        return modelAndView;
    }

    @RequestMapping("/delete-{id}")
    public String delete(@PathVariable int id) {
        int page = Utils.getPageByPosition(gamesService.getGamePosition(id));
        gamesService.delete(id);
        return redirectToPage(page);
    }

    @RequestMapping("/add")
    public String add(@ModelAttribute("game") Game game) {
        return "game-edit";
    }

    @RequestMapping("/edit-{id}")
    public ModelAndView edit(@PathVariable int id) {
        ModelAndView modelAndView = new ModelAndView("game-edit");
        modelAndView.addObject("game", gamesService.get(id));
        return modelAndView;
    }

    @Async
    @ResponseBody
    @RequestMapping(value = "/upload-image", method = RequestMethod.POST, produces = "application/json")
    public UploadedFiles uploadImage(@RequestPart("image-file") MultipartFile imagePart, HttpServletRequest request) throws IOException {
        UploadedFiles result = new UploadedFiles();
        UploadedFile file = new UploadedFile();
        result.getFiles().add(file);

        if (!"image/jpeg".equals(imagePart.getContentType())) {
            file.setError("Недопустимый формат. Загрузите картинку в формате JPEG.");
        } else if (imagePart.getSize() > MAX_IMAGE_SIZE) {
            file.setError("Недопустимый размер картинки. Загрузите картинку не более 100Кб.");
        } else {
            String id = request.getSession().getId() + "-" + UUID.randomUUID().toString();
            File imageFile = new File(Constants.Directories.TEMP_DIR, id + ".jpeg");

            BufferedImage image = ImageIO.read(imagePart.getInputStream());
            ImageIO.write(image, "JPEG", imageFile);

            File thumbFile = new File(Constants.Directories.TEMP_DIR, id + "-thumb.jpeg");
            BufferedImage thumbImage;
            if (image.getWidth() > image.getHeight()) {
                thumbImage = Scalr.resize(image, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_HEIGHT,
                        THUMB_WIDTH, THUMB_HEIGHT, Scalr.OP_ANTIALIAS);
            } else {
                thumbImage = Scalr.resize(image, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_WIDTH,
                        THUMB_WIDTH, THUMB_HEIGHT, Scalr.OP_ANTIALIAS);
            }
            thumbImage = Scalr.crop(thumbImage, THUMB_WIDTH, THUMB_HEIGHT);
            ImageIO.write(thumbImage, "JPEG", thumbFile);

            file.setId(id);
            file.setUrl(request.getContextPath() + "/images/tmp/" + id + ".jpeg");
            file.setThumbnailUrl(request.getContextPath() + "/images/tmp/" + id + "-thumb.jpeg");
        }
        return result;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("game") Game game,
                       RedirectAttributes redirectAttributes,
                       HttpServletRequest request) throws IOException {

        File tmpImageFile = new File(Constants.Directories.TEMP_DIR, game.getImage() + ".jpeg");
        File tmpThumbFile = new File(Constants.Directories.TEMP_DIR, game.getImage() + "-thumb.jpeg");

        String newImageId = game.getImage().replaceFirst("^" + request.getSession().getId() + "-", "");
        File imageFile = new File(Constants.Directories.IMAGES_DIR, newImageId + ".jpeg");
        File thumbFile = new File(Constants.Directories.IMAGES_DIR, newImageId + "-thumb.jpeg");

        Files.move(tmpImageFile.toPath(), imageFile.toPath());
        Files.move(tmpThumbFile.toPath(), thumbFile.toPath());

        game.setImage(newImageId);

        boolean updating = game.getId() != 0;
        gamesService.save(game);

        if (updating) {
            redirectAttributes.addFlashAttribute("topMessage",
                    new TopMessage(TopMessage.Status.success, "Игра \"%s\" была успешно обновлена.", game.getName()));
        } else {
            redirectAttributes.addFlashAttribute("topMessage",
                    new TopMessage(TopMessage.Status.success, "Игра \"%s\" была успешно добавлена.", game.getName()));
        }

        return redirectToPage(Utils.getPageByPosition(game.getPlays()));
    }

    private String redirectToPage(int page) {
        if (page == 1) {
            return String.format("redirect:/games");
        } else {
            return String.format("redirect:/games/%d", page);
        }
    }
}
