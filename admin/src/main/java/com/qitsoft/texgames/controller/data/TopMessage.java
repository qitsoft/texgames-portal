package com.qitsoft.texgames.controller.data;

/**
 * Created by ssolovio on 12.01.14.
 */
public class TopMessage {

    public static enum Status {

        success,
        info,
        warning,
        danger;

    }

    private Status status;

    private String message;

    public TopMessage(Status status, String messageFormat, Object... params) {
        this.status = status;
        this.message = String.format(messageFormat, params);
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
