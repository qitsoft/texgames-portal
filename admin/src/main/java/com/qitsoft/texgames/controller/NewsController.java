package com.qitsoft.texgames.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ssolovio on 14.01.14.
 */
@Controller
@RequestMapping("/news")
public class NewsController {

    @RequestMapping
    public String news() {
        return "news-list";
    }
}
