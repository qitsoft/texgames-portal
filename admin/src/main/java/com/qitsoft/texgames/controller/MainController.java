package com.qitsoft.texgames.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ssolovio on 14.12.13.
 */
@Controller
public class MainController {

    @RequestMapping("")
    public String index() {
        return "index";
    }


}
