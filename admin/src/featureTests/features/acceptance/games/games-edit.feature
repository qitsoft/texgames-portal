#language: ru
Функция: Редактирование игры
  Чтобы исправить ошибку в случае появления в игре
  Администраторы длжны иметь возможность редактировать игры

Контекст:
  Пусть мы залогинились в админку используя аккаунт администратора
  И в системе есть игры:
    |Картинка|Название    |Описание игры        |Количество игр|
    |img_2   |Морской бой |Описание морского боя|             2|
    |img_1   |Шахматы     |Описание шахмат      |             1|
  И мы нажали в меню на "Содержимое/Игры"
  И нажали на кнопку "Править" в строке игры "Шахматы"

  Сценарий: Переход на страницу редактирования игры
    Тогда должна открыться страница "Редактирование игры "Шахматы""

  Сценарий: Показ картинки
    Тогда картинка должна отображаться в поле "Картинка" с адресом "img_1"

  Сценарий: Наличие полей
    Тогда форма должна содержать поля:
      |Наименование   |Тип               |
      |Картинка       |Загрузчик картинок|
      |Название       |Текстовое         |
      |Описание       |WYSIWYG           |

  Сценарий: Значения по-умолчанию
    Тогда поля формы должны содержать следующие значения:
      |Наименование   |Значение              |
      |Картинка       |img_1                 |
      |Название       |Шахматы               |
      |Описание       |Описание шахмат       |

  Сценарий: Проверка обязятельных полей
    Если мы ввели значения в поля:
      |Наименование   |Значение |
      |Название       |         |
      |Описание       |         |
    Если мы нажали на кнопку "Сохранить игру"
    Тогда должно появится у поля "Название" сообщение "Введите название игры"
    И должно появится у поля "Описание" сообщение "Введите описание игры"

  Сценарий: Загрузка картинки неправильного формата
    Если мы загрузили картинку "orden.png" в поле "Картинка"
    И мы нажали на кнопку "Сохранить игру"
    Тогда должно появится у поля "Картинка" сообщение "Недопустимый формат. Загрузите картинку в формате JPEG."

  Сценарий: Загрузка картинки большого размера
    Если мы загрузили картинку "prey-big.jpeg" в поле "Картинка"
    И мы нажали на кнопку "Сохранить игру"
    Тогда должно появится у поля "Картинка" сообщение "Недопустимый размер картинки. Загрузите картинку не более 100Кб."

  Сценарий: Сохранение игры
    Если мы ввели значения в поля:
      |Наименование   |Значение              |
      |Картинка       |morskoy-boy.jpeg      |
      |Название       |Новые шахматы         |
      |Описание       |Новое описание шахмат |
    И мы нажали на кнопку "Сохранить игру"
    Тогда должна открыться страница "Игры"
    И должно быть сообщение "Игра "Новые шахматы" была успешно обновлена."
    А таблица должна содержать:
      |Название      |Описание              |
      |Новые шахматы |Новое описание шахмат |

  Сценарий: Сохранение положения игры
    Если мы ввели значения в поля:
      |Наименование   |Значение              |
      |Картинка       |morskoy-boy.jpeg      |
      |Название       |Новые шахматы         |
      |Описание       |Новое описание шахмат |
    И мы нажали на кнопку "Сохранить игру"
    Тогда должна открыться страница "Игры"
    И должно быть сообщение "Игра "Новые шахматы" была успешно обновлена."
    А таблица должна содержать:
      |Название      |Описание              |
      |Морской бой   |Описание морского боя |
      |Новые шахматы |Новое описание шахмат |
