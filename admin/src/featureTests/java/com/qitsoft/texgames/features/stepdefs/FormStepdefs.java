package com.qitsoft.texgames.features.stepdefs;

import com.qitsoft.texgames.core.features.stepdefs.BaseStepdefs;
import com.qitsoft.texgames.core.features.elements.FormField;
import com.qitsoft.texgames.core.features.elements.controls.FieldControl;
import com.qitsoft.texgames.core.features.elements.controls.ImageUploaderControl;
import com.qitsoft.texgames.core.features.elements.controls.TextControl;
import com.qitsoft.texgames.core.features.elements.controls.WYSIWYGControl;
import com.qitsoft.texgames.core.features.pages.FormPage;
import cucumber.api.DataTable;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Тогда;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import java.util.Map;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by ssolovio on 09.01.14.
 */
public class FormStepdefs extends BaseStepdefs {

    private static final String FIELD_NAME = "Наименование";
    private static final String FIELD_TYPE = "Тип";
    private static final String FIELD_VALUE = "Значение";
    private DataTable fieldSetMetadata;

    @Тогда("^форма должна содержать поля:$")
    public void форма_должна_содержать_поля(DataTable expectedFieldSet) throws Throwable {
        FormPage page = createPage(FormPage.class);

        for (Map<String, String> expectedFields : expectedFieldSet.asMaps()) {
            assertThat(page.getForm().getFields(), hasItem(fieldLabeledAs(expectedFields.get(FIELD_NAME))));
            assertThat(page.getForm().getFields(), hasItem(fieldOfType(expectedFields.get(FIELD_TYPE))));
        }
    }

    @Тогда("^поля формы должны содержать следующие значения:$")
    public void поля_формы_должны_содержать_следующие_значения(DataTable expectedFieldSet) throws Throwable {
        FormPage page = createPage(FormPage.class);
        fieldSetMetadata = expectedFieldSet;

        for (Map<String, String> expectedFields : expectedFieldSet.asMaps()) {
            FormField field = page.getForm().getFielsByLabel(expectedFields.get(FIELD_NAME));
            assertEquals(expectedFields.get(FIELD_VALUE), field.getControl().getValue());
        }
    }

    @Тогда("^поле \"([^\"]*)\" должно иметь фокус$")
    public void поле_должно_иметь_фокус(String fieldLabel) throws Throwable {
        FormPage page = createPage(FormPage.class);
        FormField field = page.getForm().getFielsByLabel(fieldLabel);
        assertTrue(field.getControl().isFocused(getWebDriver()));
    }

    @Если("^мы ввели значение \"([^\"]*)\" в поле \"([^\"]*)\"$")
    public void мы_ввели_значение_в_поле(String value, String label) throws Throwable {
        FormPage page = createPage(FormPage.class);
        page.getForm().getFielsByLabel(label).getControl().setValue(value);
    }

    @Если("^мы загрузили картинку \"([^\"]*)\" в поле \"([^\"]*)\"$")
    public void мы_загрузили_картинку_в_поле(String imageName, String label) throws Throwable {
        FormPage page = createPage(FormPage.class);
        page.getForm().getFielsByLabel(label).getControl().setValue(imageName);
    }

    @Если("^мы ввели значения в поля:$")
    public void мы_ввели_значения_во_все_поля(DataTable values) throws Throwable {
        FormPage page = createPage(FormPage.class);
        for (Map<String, String> row : values.asMaps()) {
            FieldControl control = page.getForm().getFielsByLabel(row.get(FIELD_NAME)).getControl();
            control.clear();
            control.setValue(row.get(FIELD_VALUE));
        }
    }

    @Если("^должно появится у поля \"([^\"]*)\" сообщение \"([^\"]*)\"$")
    public void должно_появится_у_поля_сообщение(String fieldLabel, String message) {
        FormPage page = createPage(FormPage.class);
        FormField field = page.getForm().getFielsByLabel(fieldLabel);
        field.waitToAppearErrorMessages();
        assertThat(field.getErrorMessages(), hasItem(message));
    }

    @Если("^картинка должна отображаться в поле \"([^\"]*)\" с адресом \"([^\"]*)\"$")
    public void картинка_должна_отображаться_в_поле_с_адресом(String fieldLabel, final String imageUrl) {
        FormPage page = createPage(FormPage.class);
        FormField field = page.getForm().getFielsByLabel(fieldLabel);
        ImageUploaderControl imageControl = (ImageUploaderControl) field.getControl();

        assertTrue(imageControl.getImageElement().isDisplayed());
        assertEquals(imageUrl, imageControl.getImageId());
    }

    private Matcher<? super FormField> fieldOfType(final String type) {
        Class<? extends FieldControl> expectedType = null;

        switch (type) {
            case "Текстовое":
            case "Текстовой":
                expectedType = TextControl.class;
                break;
            case "Загрузчик картинок":
                expectedType = ImageUploaderControl.class;
                break;
            case "WYSIWYG":
                expectedType = WYSIWYGControl.class;
                break;
            default: expectedType = null;
        }

        final Class<? extends FieldControl> finalExpectedType = expectedType;
        return new BaseMatcher<FormField>() {

            @Override
            public boolean matches(Object item) {
                FormField field = (FormField) item;
                return finalExpectedType != null && field.getControl() != null && finalExpectedType.isAssignableFrom(field.getControl().getClass());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("a field of type [").appendText(type).appendText("].");
            }
        };
    }

    private Matcher<? super FormField> fieldLabeledAs(final String label) {
        return new BaseMatcher<FormField>() {
            @Override
            public boolean matches(Object item) {
                FormField field = (FormField) item;
                return field.getLabel().equals(label);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("a field labeled as [").appendText(label).appendText("].");
            }
        };
    }
}
