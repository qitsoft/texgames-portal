package com.qitsoft.texgames.features.stepdefs;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.qitsoft.texgames.core.config.Constants;
import com.qitsoft.texgames.core.features.stepdefs.BaseStepdefs;
import com.qitsoft.texgames.core.features.elements.TableRow;
import com.qitsoft.texgames.core.features.pages.TablePage;
import com.qitsoft.texgames.core.features.utils.CukeFunctions;
import com.qitsoft.texgames.core.features.utils.CukeMatchers;
import com.qitsoft.texgames.core.features.utils.CukePredicates;
import cucumber.api.DataTable;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Тогда;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * Created by ssolovio on 25.12.13.
 */
public class TableStepdefs extends BaseStepdefs {

    @Тогда("^страница должна содержать таблицу$")
    public void страница_должна_содержать_список_таблицу() throws Throwable {
        TablePage page = createPage(TablePage.class);
        assertNotNull(page.getTable());
    }

    @Тогда("^таблица должна содержать поля: (.*)$")
    public void таблица_должна_содержать_поля_(List<String> list) throws Throwable {
        TablePage page = createPage(TablePage.class);
        List<String> expectedTitles = Lists.transform(list, CukeFunctions.removeQuotesTransformer);
        assertEquals(expectedTitles, page.getTable().getHeader().getSignificantTitles());
    }

    @SuppressWarnings("unchecked")
    @Тогда("^таблица должна содержать:$")
    public void таблица_должна_содержать(DataTable table) throws Throwable {
        tableShouldContain(table, null);
    }

    @SuppressWarnings("unchecked")
    @Тогда("^в первом столбце таблицы должны быть картинки:$")
    public void в_первом_столбце_таблицы_должны_быть_картинки(DataTable table) throws Throwable {
        tableShouldContain(table, new Function<String, Integer>() {
            @Override
            public Integer apply(String input) {
                if ("Картинка".equals(StringUtils.trim(input))) {
                    return 0;
                } else {
                    return null;
                }
            }
        },
        new Function<WebElement, String>() {
            @Override
            public String apply(WebElement input) {
                try {
                    WebElement imgElement = input.findElement(By.tagName("img"));
                    String src = imgElement.getAttribute("src");
                    if (StringUtils.isNotBlank(src)) {
                        return StringUtils.substringAfterLast(src, "/").replaceFirst("-thumb\\.jpeg", "");
                    }
                } catch (NoSuchElementException ex) {
                    logger.severe(ex.getLocalizedMessage());
                }
                return null;
            }
        });
    }

    @Тогда("^последний столбец таблицы должен содержать кнопки (.*)$")
    public void последний_столбец_таблицы_должен_содержать_кнопки(List<String> buttonNames) throws Throwable {
        tableButtonsContainingInLastCell(buttonNames, Predicates.<Pair<Integer, Integer>>alwaysTrue(), true);
    }

    @Тогда("^первая строка должна содержать в последнем столбце кнопки (.*)$")
    public void первая_строка_должна_содержать_в_последнем_столбце_кнопки(List<String> buttonNames) throws Throwable {
        tableButtonsContainingInLastCell(buttonNames, new Predicate<Pair<Integer, Integer>>() {
            @Override
            public boolean apply(Pair<Integer, Integer> input) {
                return input.getLeft() == 1;
            }
        }, true);
    }

    @Тогда("^последняя строка должна содержать в последнем столбце кнопки (.*)$")
    public void последняя_строка_должна_содержать_в_последнем_столбце_кнопки(List<String> buttonNames) throws Throwable {
        tableButtonsContainingInLastCell(buttonNames, new Predicate<Pair<Integer, Integer>>() {
            @Override
            public boolean apply(Pair<Integer, Integer> input) {
                return input.getLeft() == input.getRight();
            }
        }, true);
    }

    @Тогда("^первая строка должна содержать в последнем столбце неактивные кнопки (.*)$")
    public void первая_строка_не_должна_содержать_в_последнем_столбце_кнопки(List<String> buttonNames) throws Throwable {
        tableButtonsContainingInLastCell(buttonNames, new Predicate<Pair<Integer, Integer>>() {
            @Override
            public boolean apply(Pair<Integer, Integer> input) {
                return input.getLeft() == 1;
            }
        }, false);
    }

    @Тогда("^последняя строка должна содержать в последнем столбце неактивные кнопки (.*)$")
    public void последняя_строка_должна_не_содержать_в_последнем_столбце_кнопки(List<String> buttonNames) throws Throwable {
        tableButtonsContainingInLastCell(buttonNames, new Predicate<Pair<Integer, Integer>>() {
            @Override
            public boolean apply(Pair<Integer, Integer> input) {
                return input.getLeft() == input.getRight();
            }
        }, false);
    }

    @Тогда("^все строки кроме первой и последней должны содержать в последнем столбце кнопки (.*)$")
    public void все_строки_кроме_первой_и_последней_должны_содержать_в_последнем_столбце_кнопки(List<String> buttonNames) throws Throwable {
        tableButtonsContainingInLastCell(buttonNames, new Predicate<Pair<Integer, Integer>>() {
            @Override
            public boolean apply(Pair<Integer, Integer> input) {
                return input.getLeft() != 1 && input.getLeft() != input.getRight();
            }
        }, true);
    }

    @Тогда("^таблица должна содержать не белее (\\d+) строк$")
    public void таблица_должна_содержать_не_белее_строк(int num) throws Throwable {
        TablePage page = createPage(TablePage.class);
        assertThat(page.getTable().getRows().size(), Matchers.lessThanOrEqualTo(Constants.PAGE_SIZE));
    }

    @Тогда("^есть пагинатор$")
    public void есть_пагинатор() throws Throwable {
        TablePage page = createPage(TablePage.class);
        assertNotNull(page.getPaginator());
    }

    @Если("^мы перейдем на страницу (\\d+) в пагинаторе$")
    public void мы_перейдем_на_страницу_N_в_пагинаторе(int pageNum) throws Throwable {
        TablePage page = createPage(TablePage.class);
        page.getPaginator().navitageToPage(pageNum);
    }

    @Если("^мы перейдем на \"Следующую страницу\" в пагинаторе$")
    public void мы_перейдем_на_следующую_страницу_в_пагинаторе() throws Throwable {
        TablePage page = createPage(TablePage.class);
        page.getPaginator().navitageToNextPage();
    }

    @Если("^мы перейдем на \"Предыдущую страницу\" в пагинаторе$")
    public void мы_перейдем_на_предыдущую_страницу_в_пагинаторе() throws Throwable {
        TablePage page = createPage(TablePage.class);
        page.getPaginator().navitageToPrevPage();
    }

    @Тогда("^пагинатор указывает на страницу (\\d+)$")
    public void под_таблицей_пагинатор_указывает_на_страницу_N(int pageNum) throws Throwable {
        TablePage page = createPage(TablePage.class);
        assertEquals(pageNum, page.getPaginator().getActivePage());
    }

    @Тогда("^должна быть кнопка \"([^\"]*)\"$")
    public void над_таблицей_с_играми_должна_быть_кнопка(String buttonName) throws Throwable {
        TablePage page = createPage(TablePage.class);
        List<WebElement> buttons = page.getContentElement().findElements(By.tagName("button"));
        assertFalse(String.format("Unable to find button [%s].", buttonName),
                Collections2.filter(buttons, CukePredicates.isButton(buttonName)).isEmpty());
    }

    @Если("^нажали на кнопку \"([^\"]*)\" в строке (.*) \"([^\"]*)\"$")
    public void нажали_на_кнопку_в_строке_игры(String buttonName, String entityName, String title) throws Throwable {
        TablePage page = createPage(TablePage.class);
        TableRow foundRow = null;
        for (TableRow row : page.getTable().getRows()) {
            if (row.getTextCells().contains(title)) {
                foundRow = row;
                break;
            }
        }
        assertNotNull(foundRow);

        Collection<WebElement> foundButtons = Collections2.filter(
                foundRow.getWrappedElement().findElements(By.tagName("button")),
                CukePredicates.isButton(buttonName));
        assertFalse(foundButtons.isEmpty());

        foundButtons.iterator().next().click();
    }

    @Тогда("^в строке (\\d+) должна оказаться (.*) \"([^\"]*)\"$")
    public void в_строке_N_должна_оказаться(int rowNum, String entityName, String title) throws Throwable {
        TablePage page = createPage(TablePage.class);
        TableRow row = page.getTable().getRows().get(rowNum - 1);

        assertTrue(row.getTextCells().contains(title));
    }

    @Тогда("^таблица должна содержать (\\p{L}*) \"([^\"]*)\"$")
    public void таблица_должна_по_прежнему_содержать(String entityName, String title) throws Throwable {
        TablePage page = createPage(TablePage.class);
        assertThat(page.getTable().getRows(), hasItem(CukeMatchers.matchesRowWithItem(title)));
    }

    @Тогда("^таблица более не должна содержать (.*) \"([^\"]*)\"$")
    public void таблица_более_не_должна_содержать(String entityName, String title) throws Throwable {
        TablePage page = createPage(TablePage.class);
        assertThat(page.getTable().getRows(), not(hasItem(CukeMatchers.matchesRowWithItem(title))));
    }

    private void tableButtonsContainingInLastCell(List<String> buttonNames, Predicate<Pair<Integer, Integer>> rowPredicate, boolean assertExists) {
        TablePage page = createPage(TablePage.class);
        buttonNames = Lists.transform(buttonNames, CukeFunctions.removeQuotesTransformer);

        int rowNum = 0;
        int totalRows = page.getTable().getRows().size();
        for (TableRow row : page.getTable().getRows()) {
            rowNum++;
            if (!rowPredicate.apply(new ImmutablePair<Integer, Integer>(rowNum, totalRows))) {
                continue;
            }
            for (String buttonName : buttonNames) {
                WebElement lastCell = row.getCellElements().get(row.getCellElements().size() - 1);

                try {
                    List<WebElement> buttons = lastCell.findElements(By.tagName("button"));
                    Collection<WebElement> foundButtons = Collections2.filter(buttons,
                                Predicates.and(
                                        CukePredicates.isButton(buttonName),
                                        CukePredicates.isEnabled
                                )
                            );

                    if (assertExists) {
                        assertFalse(
                                String.format("Unable to find button [%s] in row #%d", buttonName, rowNum),
                                foundButtons.isEmpty());
                    } else {
                        assertTrue(
                                String.format("There is button [%s] in row #%d, but should not exist there.",
                                    buttonName, rowNum),
                                foundButtons.isEmpty());
                    }
                } catch (NoSuchElementException ex) {
                    fail(String.format("Unable to find button [%s] in row #%d", buttonName, rowNum));
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void tableShouldContain(DataTable table, Function<String, Integer> headerIndexFunction, Function<WebElement, String>... cellFunctions) {
        TablePage page = createPage(TablePage.class);

        Function<String, Integer> headerMapFunction = CukeFunctions.union(
                Lists.newArrayList(headerIndexFunction, CukeFunctions.indexOfFunction(page.getTable().getHeader().getTextCells())),
                Predicates.or(Predicates.isNull(), CukePredicates.lessThan(0))
        );

        Map<String, Integer> headerMap = Maps.toMap(table.topCells(), headerMapFunction);

        List<TableRow> actualTableRows = page.getTable().getRows();
        List<Map<String, String>> expectedTableRows = table.asMaps();
        assertNotNull(actualTableRows);
        assertThat(actualTableRows.size(), Matchers.greaterThanOrEqualTo(expectedTableRows.size()));

        Iterator<Map<String, String>> expectedRowIterator = expectedTableRows.iterator();
        Iterator<TableRow> actualRowIterator = actualTableRows.iterator();
        List<WebElement> actualCurrentRow = null;
        Map<String, String> expectedCurrentRow = null;

        List<Function<WebElement, String>> elementToTextFunctions = new ArrayList<>();
        Collections.addAll(elementToTextFunctions, cellFunctions);
        Collections.addAll(elementToTextFunctions, CukeFunctions.elementToTextTransformer);

        Function<WebElement, String> elementToTextFunction = CukeFunctions.union(elementToTextFunctions, Predicates.<String>isNull());

        while ((expectedCurrentRow != null || expectedRowIterator.hasNext()) && actualRowIterator.hasNext()) {
            if (actualCurrentRow == null) {
                actualCurrentRow = actualRowIterator.next().getCellElements();
            }
            if (expectedCurrentRow == null) {
                expectedCurrentRow = expectedRowIterator.next();
            }

            boolean foundAll = true;
            for (Map.Entry<String, String> entry : expectedCurrentRow.entrySet()) {
                int index = headerMap.get(entry.getKey());
                String actualRowCellTextByIndex = StringUtils.trim(elementToTextFunction.apply(actualCurrentRow.get(index)));
                if (!StringUtils.equals(StringUtils.trim(entry.getValue()), actualRowCellTextByIndex)) {
                    foundAll = false;
                    break;
                }
            }
            if (foundAll) {
                expectedCurrentRow = null;
                actualCurrentRow = null;
            } else {
                actualCurrentRow = null;
            }
        }
        if (actualCurrentRow == null && expectedCurrentRow != null) {
            fail(String.format("Cannot find row %s.", Iterables.toString(expectedCurrentRow.values())));
        }
    }

}
