package com.qitsoft.texgames.controller;

import com.qitsoft.texgames.controller.data.TopMessage;
import com.qitsoft.texgames.controller.data.UploadedFile;
import com.qitsoft.texgames.core.config.Constants;
import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.service.GamesService;
import com.qitsoft.texgames.service.model.PagedResult;
import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by ssolovio on 29.12.13.
 */
@RunWith(MockitoJUnitRunner.class)
public class GameControllerTest {

    private static final long TOO_BIG_IMAGE_SIZE = 1024 * 1024L;
    private static final int THUMB_WIDTH = 100;
    private static final int THUMB_HEIGHT = 100;
    private static final String THUMB_FORMAT = "JPEG";
    private static final int SAMPLE_GAME_ID = 10;
    @Spy
    @InjectMocks
    private GamesController controller = new GamesController();

    @Mock
    private GamesService gamesService;

    @Mock
    private Game game1;

    @Mock
    private Game game2;

    @Mock
    private PagedResult<Game> pagedResult1;

    @Mock
    private PagedResult<Game> pagedResult2;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession session;

    @Mock
    private RedirectAttributes redirectAttributes;

    @Before
    public void setUp() throws IOException {
        FileUtils.cleanDirectory(Constants.Directories.TEMP_DIR);
        File[] files = Constants.Directories.IMAGES_DIR.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("image");
            }
        });
        for (File file : files) {
            file.delete();
        }

        when(request.getSession()).thenReturn(session);
        when(request.getContextPath()).thenReturn("/admin");
        when(session.getId()).thenReturn("SESSIONID");
    }

    @Test
    public void testListPageReturnsGamesView() {
        assertEquals("games-list", controller.games().getViewName());
    }

    @Test
    public void testIndexPageReturnsGamesList() {
        when(gamesService.list(eq(1), eq(Constants.PAGE_SIZE))).thenReturn(pagedResult1);
        Map<String, Object> model = controller.games().getModel();

        assertNotNull(model);
        assertThat(model, hasKey("games"));
        assertThat(model.get("games"), Matchers.<Object>equalTo(pagedResult1));
    }

    @Test
    public void testIndexPageReturnsFirstPage() {
        controller.games();
        verify(gamesService).list(eq(1), eq(Constants.PAGE_SIZE));
    }

    @Test
    public void testIndexPageForwardsToPaged() {
        controller.games();
        verify(controller).games(eq(1));
    }

    @Test
    public void testPagedPageReturnsTheRequestedPage() {
        when(gamesService.list(eq(1), eq(Constants.PAGE_SIZE))).thenReturn(pagedResult1);
        when(gamesService.list(eq(2), eq(Constants.PAGE_SIZE))).thenReturn(pagedResult2);

        assertThat(controller.games(1).getModel().get("games"), Matchers.<Object>equalTo(pagedResult1));
        assertThat(controller.games(2).getModel().get("games"), Matchers.<Object>equalTo(pagedResult2));
    }

    @Test
    public void testDelete() {
        when(gamesService.getGamePosition(eq(1))).thenReturn(1);
        String result = controller.delete(1);

        verify(gamesService).delete(1);
        assertEquals("redirect:/games", result);
    }

    @Test
    public void testDeleteSecondPage() {
        when(gamesService.getGamePosition(eq(1))).thenReturn(Constants.PAGE_SIZE + 1);
        String result = controller.delete(1);

        verify(gamesService).delete(1);
        assertEquals("redirect:/games/2", result);
    }

    @Test
    public void testAddGameViewName() {
        assertEquals("game-edit", controller.add(game1));
    }

    @Test
    public void testUploadWrongFormatImage() throws IOException {
        MultipartFile multipartFile = mock(MultipartFile.class);
        when(multipartFile.getContentType()).thenReturn("image/png");
        when(request.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("SESSIONID");

        UploadedFile file = controller.uploadImage(multipartFile, request).getFiles().get(0);

        assertNotNull(file.getError());
        assertFalse(file.getError().trim().isEmpty());
    }

    @Test
    public void testUploadTooBigImage() throws IOException {
        MultipartFile multipartFile = mock(MultipartFile.class);
        when(multipartFile.getContentType()).thenReturn("image/jpeg");
        when(multipartFile.getSize()).thenReturn(TOO_BIG_IMAGE_SIZE);
        when(request.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("SESSIONID");

        UploadedFile file = controller.uploadImage(multipartFile, request).getFiles().get(0);

        assertNotNull(file.getError());
        assertFalse(file.getError().trim().isEmpty());
    }

    @Test
    public void testUploadImage() throws IOException {
        MultipartFile multipartFile = new MockMultipartFile("morskoy-boy.jpeg", "morskoy-boy.jpeg", "image/jpeg",
                getClass().getClassLoader().getResourceAsStream("morskoy-boy.jpeg"));
        when(request.getSession()).thenReturn(session);
        when(request.getContextPath()).thenReturn("/admin");
        when(session.getId()).thenReturn("SESSIONID");

        UploadedFile file = controller.uploadImage(multipartFile, request).getFiles().get(0);

        assertThat(file.getId(), Matchers.startsWith("SESSIONID-"));
        File imageFile = new File(Constants.Directories.TEMP_DIR, file.getId() + ".jpeg");
        File thumbFile = new File(Constants.Directories.TEMP_DIR, file.getId() + "-thumb.jpeg");

        assertEquals("/admin/images/tmp/" + imageFile.getName(), file.getUrl());
        assertEquals("/admin/images/tmp/" + thumbFile.getName(), file.getThumbnailUrl());
        assertTrue(imageFile.exists());
        assertTrue(thumbFile.exists());
    }

    @Test
    public void testTmpThumbImageSizeAndFormat() throws IOException {
        MultipartFile multipartFile = new MockMultipartFile("morskoy-boy.jpeg", "morskoy-boy.jpeg", "image/jpeg",
                getClass().getClassLoader().getResourceAsStream("morskoy-boy.jpeg"));
        when(request.getSession()).thenReturn(session);
        when(request.getContextPath()).thenReturn("/admin");
        when(session.getId()).thenReturn("SESSIONID");

        UploadedFile file = controller.uploadImage(multipartFile, request).getFiles().get(0);

        File thumbFile = new File(Constants.Directories.TEMP_DIR, file.getId() + "-thumb.jpeg");
        ImageInputStream input = ImageIO.createImageInputStream(thumbFile);
        ImageReader reader = ImageIO.getImageReaders(input).next();
        reader.setInput(input);

        assertEquals(THUMB_FORMAT, reader.getFormatName());
        assertEquals(THUMB_WIDTH, reader.getWidth(0));
        assertEquals(THUMB_HEIGHT, reader.getHeight(0));
    }

    @Test
    public void testSaveImageOnAddAndUpdateModel() throws IOException {
        SavePreparedData data = new SavePreparedData();

        controller.save(data.game, redirectAttributes, request);

        assertFalse(data.tmpImageFile.exists());
        assertFalse(data.tmpThumbFile.exists());
        assertTrue(data.imageFile.exists());
        assertTrue(data.thumbFile.exists());
        assertEquals(data.imageContent, com.google.common.io.Files.toString(data.imageFile, Charset.forName("UTF8")));
        assertEquals(data.imageThumbContent, com.google.common.io.Files.toString(data.thumbFile, Charset.forName("UTF8")));
        assertEquals(data.simpleImageId, data.game.getImage());
    }

    @Test
    public void testSaveInvokesService() throws IOException {
        SavePreparedData data = new SavePreparedData();

        controller.save(data.game, redirectAttributes, request);

        verify(gamesService).save(eq(data.game));
    }

    @Test
    public void testSaveRedirectsToList() throws IOException {
        SavePreparedData data = new SavePreparedData();

        assertThat(controller.save(data.game, redirectAttributes, request), Matchers.startsWith("redirect:/games"));
    }

    @Test
    public void testSaveSetPlaysToZero() throws IOException {
        SavePreparedData data = new SavePreparedData();
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ((Game) invocation.getArguments()[0]).setPlays(0);
                return null;
            }
        }).when(gamesService).save(eq(data.game));

        controller.save(data.game, redirectAttributes, request);

        assertEquals(0, data.game.getPlays());
    }

    @Test
    public void testSaveRedirectsToLastPage() throws IOException {
        SavePreparedData data = new SavePreparedData();
        data.game.setPlays(Constants.PAGE_SIZE * 2 + 1);

        assertEquals("redirect:/games/3", controller.save(data.game, redirectAttributes, request));
    }

    @Test
    public void testSaveAddsFlashSuccess() throws IOException {
        SavePreparedData data = new SavePreparedData();
        data.game.setPlays(Constants.PAGE_SIZE * 2 + 1);

        controller.save(data.game, redirectAttributes, request);

        verify(redirectAttributes).addFlashAttribute(eq("topMessage"), Mockito.any(TopMessage.class));
    }

    @Test
    public void testEditViewName() {
        ModelAndView modelAndView = controller.edit(SAMPLE_GAME_ID);

        assertNotNull(modelAndView);
        assertEquals("game-edit", modelAndView.getViewName());
    }

    @Test
    public void testEditReturnGame() {
        when(gamesService.get(eq(SAMPLE_GAME_ID))).thenReturn(game1);
        ModelAndView modelAndView = controller.edit(SAMPLE_GAME_ID);

        Object gameObject = modelAndView.getModel().get("game");
        assertNotNull(gameObject);
        assertThat(gameObject, instanceOf(Game.class));
    }

    private Game prepareSampleGame(String simpleImageId) {
        Game game = new Game();
        game.setName("gameName");
        game.setDescription("gameDesc");
        game.setImage("SESSIONID-" + simpleImageId);
        return game;
    }

    private static class SavePreparedData {

        private String simpleImageId = "image";

        private Game game;

        private File tmpImageFile;

        private File tmpThumbFile;

        private File imageFile;

        private File thumbFile;

        private String imageContent;

        private String imageThumbContent;

        public SavePreparedData() throws IOException {
            simpleImageId = "image";

            game = new Game();
            game.setName("gameName");
            game.setDescription("gameDesc");
            game.setImage("SESSIONID-" + simpleImageId);

            tmpImageFile = new File(Constants.Directories.TEMP_DIR, game.getImage() + ".jpeg");
            tmpThumbFile = new File(Constants.Directories.TEMP_DIR, game.getImage() + "-thumb.jpeg");
            imageFile = new File(Constants.Directories.IMAGES_DIR, simpleImageId + ".jpeg");
            thumbFile = new File(Constants.Directories.IMAGES_DIR, simpleImageId + "-thumb.jpeg");

            imageContent = "simple image";
            imageThumbContent = "simple thumb image";
            com.google.common.io.Files.write(imageContent, tmpImageFile, Charset.forName("UTF8"));
            com.google.common.io.Files.write(imageThumbContent, tmpThumbFile, Charset.forName("UTF8"));
        }
    }
}
