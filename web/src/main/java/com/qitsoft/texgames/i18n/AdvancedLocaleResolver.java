package com.qitsoft.texgames.i18n;

import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by ssolovio on 17.12.13.
 */
public class AdvancedLocaleResolver implements LocaleResolver {

    private static Logger logger = Logger.getLogger(AdvancedLocaleResolver.class.getName());

    private List<LocaleResolver> resolvers = new ArrayList<>();

    private Locale fallbackLocale;

    private Locale defaultLocale;

    public List<LocaleResolver> getResolvers() {
        return resolvers;
    }

    public void setResolvers(List<LocaleResolver> resolvers) {
        this.resolvers = resolvers;
    }

    public Locale getFallbackLocale() {
        return fallbackLocale;
    }

    public void setFallbackLocale(Locale fallbackLocale) {
        this.fallbackLocale = fallbackLocale;
    }

    public Locale getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(Locale defaultLocale) {
        this.defaultLocale = defaultLocale;
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        Locale result = null;
        for (LocaleResolver resolver : resolvers) {
            result = resolver.resolveLocale(request);
            if (result != null && !result.equals(fallbackLocale)) {
                return result;
            }
        }

        if (result == null && defaultLocale != null) {
            return defaultLocale;
        } else {
            return result;
        }
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        for (LocaleResolver resolver : resolvers) {
            try {
                resolver.setLocale(request, response, locale);
                return;
            } catch (Throwable ex) {
                logger.log(Level.WARNING, String.format("While setting the locale [%s] raised an exception.", resolver.getClass().getName()), ex);
            }
        }
    }
}
