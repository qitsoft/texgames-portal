package com.qitsoft.texgames.controller;


import com.qitsoft.texgames.service.GamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ssolovio on 14.12.13.
 */
@Controller
public class MainController {

    private static final int GAMES_PER_PAGE = 3;

    @Autowired
    private GamesService gamesService;

    @RequestMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("games", gamesService.list(1, GAMES_PER_PAGE).getItems());
        return modelAndView;
    }


    @RequestMapping("/forum")
    public String forum() {
        return "forum";
    }

    @RequestMapping("/contacts")
    public String contacts() {
        return "contacts";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/signup")
    public String signup() {
        return "signup";
    }
}
