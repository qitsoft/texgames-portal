package com.qitsoft.texgames.controller;

import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.service.GamesService;
import com.qitsoft.texgames.service.model.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ssolovio on 14.01.14.
 */
@Controller
@RequestMapping("/games")
public class GamesController {

    private static final int PAGE_SIZE = 5;

    @Autowired
    private GamesService gamesService;

    @RequestMapping
    public ModelAndView games() {
        return games(1);
    }

    @RequestMapping("/{page}")
    public ModelAndView games(@PathVariable int page) {
        ModelAndView modelAndView = new ModelAndView("games");
        PagedResult<Game> result = gamesService.list(page, PAGE_SIZE);
        modelAndView.addObject("data", result);
        return modelAndView;
    }

    @RequestMapping("/play-{id}")
    public String play(@PathVariable int id) {
        gamesService.incrementPlays(id);
        return "game-play";
    }

}
