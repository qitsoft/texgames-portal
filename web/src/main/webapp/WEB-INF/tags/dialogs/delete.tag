<%@tag pageEncoding="UTF-8" %>
<%@attribute name="title" %>
<div class="modal fade danger" id="deleteDialog" tabindex="-1" role="dialog" aria-labelledby="deleteDialogTitle" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deleteDialogTitle" style="color: white;">${title}</h4>
            </div>
            <div class="modal-body">
                <jsp:doBody />
            </div>
            <div class="modal-footer">
                <button id="deleteDialogOkButton" type="button" class="btn btn-danger">Да</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>