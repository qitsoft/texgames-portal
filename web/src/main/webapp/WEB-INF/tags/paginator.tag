<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="data" type="com.qitsoft.texgames.service.model.PagedResult" %>
<%@attribute name="pagePath" type="java.lang.String" %>
<%@attribute name="id" %>
<c:if test="${data.totalPages > 1}">
    <ul id="${id}" class="pagination">
        <c:choose>
            <c:when test="${data.prevPage == 1}">
                <li <c:if test="${data.firstPage}"> class="disabled"</c:if>><a href="${baseUrl}/${pagePath}/">&laquo;</a></li>
            </c:when>
            <c:otherwise>
                <li <c:if test="${data.firstPage}"> class="disabled"</c:if>><a href="${baseUrl}/${pagePath}/${data.prevPage}">&laquo;</a></li>
            </c:otherwise>
        </c:choose>
        <c:choose>
            <c:when test="${data.currentPage == 1}">
                <li class="active"><a href="${baseUrl}/${pagePath}">1 <span class="sr-only">(current)</span></a></li>
            </c:when>
            <c:otherwise>
                <li><a href="${baseUrl}/${pagePath}">1</a></li>
            </c:otherwise>
        </c:choose>
        <c:forEach var="page" begin="2" end="${data.totalPages}">
            <c:choose>
                <c:when test="${data.currentPage == page}">
                    <li class="active"><a href="${baseUrl}/${pagePath}/${page}">${page} <span class="sr-only">(current)</span></a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="${baseUrl}/${pagePath}/${page}">${page}</a></li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <li <c:if test="${data.lastPage}"> class="disabled"</c:if>><a href="${baseUrl}/${pagePath}/${data.nextPage}">&raquo;</a></li>
    </ul>
</c:if>