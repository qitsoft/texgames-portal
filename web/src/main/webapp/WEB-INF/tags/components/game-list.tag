<%@tag pageEncoding="UTF-8" import="java.util.List, com.qitsoft.texgames.model.Game" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@attribute name="games" type="List<Game>" %>
<table id="games" border="0" width="100%">
    <c:forEach var="game" items="${games}">
        <tr class="game">
            <td width="110"><img class="thumb" src="${imageBaseUrl}/${game.image}-thumb.jpeg" width="100" height="100"/></td>
            <td>
                <h2 class="game-name">${game.name}</h2>
                <div class="game-description">${game.description}</div>
                <div style="float: right; margin-right: 50px;">
                    <a href="#" class="game-link"><spring:message code="game.linkTo" /></a>
                </div>
            </td>
            <td>
                <button class="btn game-play-btn" onclick="call('games/play-${game.id}');"><spring:message code="game.play"/></button>
            </td>
        </tr>
    </c:forEach>
</table>
