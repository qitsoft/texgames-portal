<%@tag pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<nav id="mainmenu" class="navbar navbar-default" role="navigation">
    <ul class="nav navbar-nav">
        <li><a href="${baseUrl}/"><spring:message code="menu.main" /></a></li>
        <li><a href="${baseUrl}/games"><spring:message code="menu.games" /></a></li>
        <li><a href="${baseUrl}/forum"><spring:message code="menu.forum" /></a></li>
        <li><a href="${baseUrl}/contacts"><spring:message code="menu.contacts" /></a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="?lang=ru">ru</a></li>
        <li><a href="?lang=en">en</a></li>
        <li><a href="${baseUrl}/login"><spring:message code="menu.login" /></a></li>
        <li><a href="${baseUrl}/signup"><spring:message code="menu.signup" /></a></li>
    </ul>
</nav>