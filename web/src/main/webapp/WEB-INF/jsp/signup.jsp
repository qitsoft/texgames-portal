<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<spring:message var="_title" code="menu.signup" />
<t:main title="${_title}">
    <spring:message code="signup.content" />
</t:main>
