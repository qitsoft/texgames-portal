<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/components" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<spring:message var="_title" code="menu.main" />
<t:main title="${_title}" pageClass="index">
    <div id="games">
        <c:forEach var="game" items="${games}">
            <div class="game">
                <a class="game-thumb" href="${baseUrl}/games/play-${game.id}" title="${game.name}"><img class="thumb" src="${imageBaseUrl}/${game.image}-thumb.jpeg" width="100" height="100"/></a>
                <a href="#" class="game-link"><spring:message code="game.linkTo" /></a>
            </div>
        </c:forEach>
    </div>
</t:main>
