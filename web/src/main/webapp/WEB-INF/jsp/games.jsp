<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="cc" tagdir="/WEB-INF/tags/components" %>
<spring:message var="_title" code="menu.games" />
<t:main title="${_title}">
    <cc:game-list games="${data.items}" />
    <t:paginator id="games-paginator" data="${data}" pagePath="games" />
</t:main>
