package com.qitsoft.texgames.features;

import com.mysema.query.jpa.hibernate.HibernateQuery;
import com.qitsoft.texgames.core.features.DatabaseManager;
import com.qitsoft.texgames.core.features.stepdefs.BaseStepdefs;
import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.model.QGame;
import com.qitsoft.texgames.pages.GamesPage;
import cucumber.api.DataTable;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Тогда;
import org.hibernate.Session;
import org.openqa.selenium.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.Map;

import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by ssolovio on 14.01.14.
 */
public class GameStepdefs extends BaseStepdefs {

    private static final String LIST_THUMB = "Картинка";

    private static final String LIST_NAME = "Название";

    private static final String LIST_DESC = "Описание";

    @Autowired
    private DatabaseManager databaseManager;

    @Тогда("^список игр должен содержать:$")
    public void список_игр_должен_содержать(DataTable table) {
        GamesPage page = createPage(GamesPage.class);
        Iterator<GamesPage.GameItem> gameItemIterator = page.getGames().iterator();
        Iterator<Map<String, String>> expectedIterator = table.asMaps().iterator();

        GamesPage.GameItem gameItem = null;
        Map<String, String> expectedItem = null;
        while (gameItemIterator.hasNext() && expectedIterator.hasNext()) {
            gameItem = gameItemIterator.next();
            if (expectedItem == null) {
                expectedItem = expectedIterator.next();
            }

            if (expectedItem.get(LIST_THUMB).equals(gameItem.getThumb().getSrc())
                    && expectedItem.get(LIST_NAME).equals(gameItem.getName().getText())
                    && expectedItem.get(LIST_DESC).equals(gameItem.getDescription().getText())) {
                expectedItem = null;
            }
        }

        Map<String, String> itemNotFound = null;
        if (expectedItem != null) {
            itemNotFound = expectedItem;
        } else if (expectedIterator.hasNext()) {
            itemNotFound = expectedIterator.next();
        }

        if (itemNotFound != null) {
            StringBuilder expectedBuf = new StringBuilder();
            for (Map.Entry<String, String> entry : itemNotFound.entrySet()) {
                expectedBuf.append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
            }

            StringBuilder actualBuf = new StringBuilder();
            for (GamesPage.GameItem item : page.getGames()) {
                actualBuf.append(item.getThumb().getSrc()).append(" | ")
                        .append(item.getName()).append(" | ")
                        .append(item.getDescription()).append(" |")
                .append("\n");
            }

            fail(String.format("The expected and actual games lists differs. Could not find the following item:\n"
                    + "%s\n\n"
                    + "Actual list is:\n"
                    + "%s",
                    expectedBuf.toString(), actualBuf.toString()));
        }
    }

    @Тогда("^каждый элемент списка игр должен содержать кнопку \"(.*)\"$")
    public void каждый_элемент_списка_игр_должен_содержать_кнопку(String name) {
        GamesPage page = createPage(GamesPage.class);
        for (GamesPage.GameItem gameItem : page.getGames()) {
            try {
                assertEquals(name, gameItem.getPlayButon().getText());
            } catch (NoSuchElementException e) {
                fail(String.format("There is no button [%s] for game [%s].", name, gameItem.getName()));
            }
        }
    }

    @Тогда("^каждый элемент списка игр должен содержать ссылку \"(.*)\"$")
    public void каждый_элемент_списка_игр_должен_содержать_ссылку(String name) {
        GamesPage page = createPage(GamesPage.class);
        for (GamesPage.GameItem gameItem : page.getGames()) {
            try {
                assertEquals(name, gameItem.getLinkTo().getText());
            } catch (NoSuchElementException e) {
                fail(String.format("There is no link [%s] for game [%s].", name, gameItem.getName()));
            }
        }
    }

    @Тогда("^список игр должен содержать не белее (\\d+) строк$")
    public void список_игр_должен_содержать_не_белее_N_строк(int num) throws InterruptedException {
        GamesPage page = createPage(GamesPage.class);
        assertThat(page.getGames().size(), lessThanOrEqualTo(num));
    }

    @Тогда("^есть пагинатор$")
    public void есть_пагинатор() throws Throwable {
        GamesPage page = createPage(GamesPage.class);
        try {
            assertTrue(page.getPaginator().isVisible());
        } catch (Exception e) {
            fail("There is no paginator.");
        }
    }

    @Если("^мы перейдем на страницу (\\d+) в пагинаторе игр$")
    public void мы_перейдем_на_страницу_N_в_пагинаторе_игр(int pageNum) {
        GamesPage page = createPage(GamesPage.class);
        page.getPaginator().navitageToPage(pageNum);
    }

    @Если("^мы перейдем на следующую страницу в пагинаторе игр$")
    public void мы_перейдем_на_следующую_страницу_в_пагинаторе_игр() {
        GamesPage page = createPage(GamesPage.class);
        page.getPaginator().navitageToNextPage();
    }

    @Если("^мы перейдем на предыдущую страницу в пагинаторе игр$")
    public void мы_перейдем_на_предыдущую_страницу_в_пагинаторе_игр() {
        GamesPage page = createPage(GamesPage.class);
        page.getPaginator().navitageToPrevPage();
    }

    @Тогда("^пагинатор игр указывает на страницу (\\d+)$")
    public void пагинатор_игр_указывает_на_страницу(int pageNum) {
        GamesPage page = createPage(GamesPage.class);
        assertEquals(pageNum, page.getPaginator().getActivePage());
    }

    @Тогда("^первый элемент в списке игр указывает на игру с порядковым номером (\\d+) после сортировки$")
    public void первый_элемент_в_списке_игр_указывает_на_игру_с_порядковым_номером_N_после_сортировки(int position) {
        GamesPage page = createPage(GamesPage.class);

        Session session = databaseManager.getSession();

        Game game = new HibernateQuery(session)
                .from(QGame.game)
                .orderBy(QGame.game.plays.desc())
                .limit(1)
                .offset(position - 1)
                .uniqueResult(QGame.game);
        assertNotNull(game);

        GamesPage.GameItem firstItem = page.getGames().get(0);

        assertEquals(game.getName(), firstItem.getName().getText());
        assertEquals(game.getDescription(), firstItem.getDescription().getText());
        assertEquals(game.getImage(), firstItem.getThumb().getSrc());
    }
}
