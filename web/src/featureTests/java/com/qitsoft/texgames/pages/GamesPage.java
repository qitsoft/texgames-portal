package com.qitsoft.texgames.pages;

import com.qitsoft.texgames.core.features.CukeConfig;
import com.qitsoft.texgames.core.features.elements.AbstractContainer;
import com.qitsoft.texgames.core.features.elements.BaseTextualElement;
import com.qitsoft.texgames.core.features.elements.Button;
import com.qitsoft.texgames.core.features.elements.Heading;
import com.qitsoft.texgames.core.features.elements.Link;
import com.qitsoft.texgames.core.features.elements.Paginator;
import com.qitsoft.texgames.core.features.elements.TextualElement;
import com.qitsoft.texgames.core.features.elements.Thumbnail;
import com.qitsoft.texgames.core.features.pages.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by ssolovio on 14.01.14.
 */
public class GamesPage extends Page {

    @FindBy(css = "table#games tr")
    private List<GameItem> games;

    @FindBy(css = "ul#games-paginator.pagination")
    private Paginator paginator;

    public GamesPage(WebDriver webDriver, CukeConfig config) {
        super(webDriver, config);
    }

    public List<GameItem> getGames() {
        return games;
    }

    public Paginator getPaginator() {
        return paginator;
    }

    public static class GameItem extends AbstractContainer {

        @FindBy(css = "td img.thumb")
        private Thumbnail thumb;

        @FindBy(css = "h2.game-name")
        private Heading name;

        @FindBy(css = "div.game-description")
        private BaseTextualElement description;

        @FindBy(css = "a.game-link")
        private Link linkTo;

        @FindBy(css = "button.game-play-btn")
        private Button playButon;

        public GameItem(WebDriver webDriver, WebElement wrappedElement) {
            super(webDriver, wrappedElement);
        }

        public Thumbnail getThumb() {
            return thumb;
        }

        public TextualElement getName() {
            return name;
        }

        public TextualElement getDescription() {
            return description;
        }

        public Link getLinkTo() {
            return linkTo;
        }

        public Button getPlayButon() {
            return playButon;
        }
    }
}
