package com.qitsoft.texgames.i18n;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * Created by ssolovio on 17.12.13.
 */
@RunWith(MockitoJUnitRunner.class)
public class AdvancedLocaleResolverTest {

    private AdvancedLocaleResolver advancedLocaleResolver;

    @Mock
    private LocaleResolver localeResolver1;

    @Mock
    private LocaleResolver localeResolver2;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Before
    public void setUp() {
        advancedLocaleResolver = new AdvancedLocaleResolver();
        advancedLocaleResolver.getResolvers().add(localeResolver1);
        advancedLocaleResolver.getResolvers().add(localeResolver2);
    }

    @Test
    public void testResolversArrayNotNull() {
        assertNotNull(new AdvancedLocaleResolver().getResolvers());
    }

    @Test
    public void testResolveLocaleFromFirst() {
        when(localeResolver1.resolveLocale(request)).thenReturn(Locale.FRENCH);
        assertEquals(Locale.FRENCH, advancedLocaleResolver.resolveLocale(request));
        verify(localeResolver2, never()).resolveLocale(eq(request));
    }

    @Test
    public void testResolveLocaleFromSecondIfFirstNull() {
        when(localeResolver1.resolveLocale(request)).thenReturn(null);
        when(localeResolver2.resolveLocale(request)).thenReturn(Locale.FRENCH);
        assertEquals(Locale.FRENCH, advancedLocaleResolver.resolveLocale(request));
    }

    @Test
    public void testResolveLocaleFromSecondIfFirstEqualsFallbackLocale() {
        when(localeResolver1.resolveLocale(request)).thenReturn(Locale.ENGLISH);
        when(localeResolver2.resolveLocale(request)).thenReturn(Locale.FRENCH);
        advancedLocaleResolver.setFallbackLocale(Locale.ENGLISH);
        assertEquals(Locale.FRENCH, advancedLocaleResolver.resolveLocale(request));
    }

    @Test
    public void testResolveLocaleFromSecondIfAllEqualsFallbackLocale() {
        when(localeResolver1.resolveLocale(request)).thenReturn(Locale.ENGLISH);
        when(localeResolver2.resolveLocale(request)).thenReturn(Locale.ENGLISH);
        advancedLocaleResolver.setFallbackLocale(Locale.ENGLISH);
        assertEquals(Locale.ENGLISH, advancedLocaleResolver.resolveLocale(request));
        verify(localeResolver1).resolveLocale(request);
        verify(localeResolver2).resolveLocale(request);
    }

    @Test
    public void testResolveLocaleFromSecondIfAllEqualsNull() {
        when(localeResolver1.resolveLocale(request)).thenReturn(null);
        when(localeResolver2.resolveLocale(request)).thenReturn(null);
        advancedLocaleResolver.setFallbackLocale(Locale.ENGLISH);
        assertNull(advancedLocaleResolver.resolveLocale(request));
        verify(localeResolver1).resolveLocale(request);
        verify(localeResolver2).resolveLocale(request);
    }

    @Test
    public void testResolveToDefaultLocaleFromSecondIfAllEqualsNull() {
        when(localeResolver1.resolveLocale(request)).thenReturn(null);
        when(localeResolver2.resolveLocale(request)).thenReturn(null);
        advancedLocaleResolver.setFallbackLocale(Locale.ENGLISH);
        advancedLocaleResolver.setDefaultLocale(Locale.FRENCH);
        assertEquals(Locale.FRENCH, advancedLocaleResolver.resolveLocale(request));
        verify(localeResolver1).resolveLocale(request);
        verify(localeResolver2).resolveLocale(request);
    }

    @Test
    public void testSetLocaleToFirst() {
        advancedLocaleResolver.setLocale(request, response, Locale.FRENCH);
        verify(localeResolver1).setLocale(eq(request), eq(response), eq(Locale.FRENCH));
        verify(localeResolver2, never()).setLocale(eq(request), eq(response), any(Locale.class));
    }

    @Test
    public void testSetLocaleToSecondWhenFirstFails() {
        doThrow(new RuntimeException()).when(localeResolver1).setLocale(eq(request), eq(response), any(Locale.class));
        advancedLocaleResolver.setLocale(request, response, Locale.FRENCH);
        verify(localeResolver1).setLocale(eq(request), eq(response), eq(Locale.FRENCH));
        verify(localeResolver2).setLocale(eq(request), eq(response), eq(Locale.FRENCH));
    }
}
