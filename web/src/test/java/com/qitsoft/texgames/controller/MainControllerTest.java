package com.qitsoft.texgames.controller;

import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.service.GamesService;
import com.qitsoft.texgames.service.model.PagedResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ssolovio on 15.01.14.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainControllerTest {

    private static final int GAMES_PER_PAGE = 3;

    @Spy
    @InjectMocks
    private MainController controller = new MainController();

    @Mock
    private GamesService gamesService;

    @Test
    public void testIndexReturnsIndexView() {
        when(gamesService.list(anyInt(), anyInt())).thenReturn(new PagedResult<Game>());

        ModelAndView modelAndView = controller.index();
        assertEquals("index", modelAndView.getViewName());
    }

    @Test
    public void testIndexReturnsThreeGamesDirectlyFromService() {
        List<Game> games = new ArrayList<>();
        PagedResult<Game> gamesPagedResult = new PagedResult<>();
        gamesPagedResult.setItems(games);
        when(gamesService.list(anyInt(), anyInt())).thenReturn(gamesPagedResult);

        ModelAndView modelAndView = controller.index();
        Object gamesObject = modelAndView.getModel().get("games");

        assertNotNull(gamesObject);
        assertThat(gamesObject, instanceOf(List.class));
        assertSame(games, gamesObject);
        verify(gamesService).list(eq(1), eq(GAMES_PER_PAGE));
    }
}
