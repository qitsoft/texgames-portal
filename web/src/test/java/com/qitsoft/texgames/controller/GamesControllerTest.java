package com.qitsoft.texgames.controller;

import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.service.GamesService;
import com.qitsoft.texgames.service.model.PagedResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ssolovio on 15.01.14.
 */
@RunWith(MockitoJUnitRunner.class)
public class GamesControllerTest {

    private static final int PAGE_SIZE = 5;

    private static final int SAMPLE_GAME_ID = 2;

    @Spy
    @InjectMocks
    private GamesController controller = new GamesController();

    @Mock
    private GamesService gamesService;

    @Test
    public void testGamesReturnsGameListView() {
        ModelAndView modelAndView = controller.games();

        assertNotNull(modelAndView);
        assertEquals("games", modelAndView.getViewName());
    }

    @Test
    public void testGamesInvokesOverloadedForFirstPage() {
        controller.games();
        verify(controller).games(eq(1));
    }

    @Test
    public void testGamesInkovesService() {
        PagedResult<Game> gamesPagedResult = new PagedResult<>();
        when(gamesService.list(anyInt(), anyInt())).thenReturn(gamesPagedResult);

        ModelAndView modelAndView = controller.games(SAMPLE_GAME_ID);
        Object result = modelAndView.getModel().get("data");

        assertNotNull(result);
        assertSame(gamesPagedResult, result);
        verify(gamesService).list(eq(SAMPLE_GAME_ID), eq(PAGE_SIZE));
    }

    @Test
    public void testPlayReturnsGamePlayView() {
        assertEquals("game-play", controller.play(SAMPLE_GAME_ID));
    }

    @Test
    public void testPlayInvokesServiceToIncrementPlaysOfTheGame() {
        controller.play(SAMPLE_GAME_ID);
        verify(gamesService).incrementPlays(eq(SAMPLE_GAME_ID));
    }
}
