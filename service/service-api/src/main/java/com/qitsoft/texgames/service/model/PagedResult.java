package com.qitsoft.texgames.service.model;

import java.util.List;

/**
 * Created by ssolovio on 30.12.13.
 */
public class PagedResult<T> {

    private int currentPage;

    private int totalPages;

    private long totalItems;

    private int pageSize;

    private List<T> items;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
        updateTotalPages();
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
        updateTotalPages();
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public boolean isFirstPage() {
        return currentPage == 1;
    }

    public boolean isLastPage() {
        return currentPage == totalPages;
    }

    public int getNextPage() {
        return currentPage + 1;
    }

    public int getPrevPage() {
        return currentPage - 1;
    }

    private void updateTotalPages() {
        if (pageSize == 0) {
            return;
        }

        this.totalPages = (int) (totalItems / pageSize);
        if ((totalItems % pageSize) > 0) {
            totalPages++;
        }
    }
}
