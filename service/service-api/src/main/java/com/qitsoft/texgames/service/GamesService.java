package com.qitsoft.texgames.service;

import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.service.model.PagedResult;

/**
 * Created by ssolovio on 29.12.13.
 */
public interface GamesService {

    PagedResult<Game> list(int page, int pageSize);

    void delete(int id);

    int getGamePosition(int id);

    void save(Game game);

    Game get(int id);

    void incrementPlays(int id);
}
