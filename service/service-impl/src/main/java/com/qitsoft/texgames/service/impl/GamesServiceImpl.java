package com.qitsoft.texgames.service.impl;

import com.qitsoft.texgames.dao.GamesDao;
import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.service.GamesService;
import com.qitsoft.texgames.service.model.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ssolovio on 29.12.13.
 */
@Service
public class GamesServiceImpl implements GamesService {

    @Autowired
    private GamesDao gamesDao;

    @Override
    public PagedResult<Game> list(int page, int pageSize) {
        PagedResult<Game> result = new PagedResult<>();
        result.setItems(gamesDao.list((page - 1) * pageSize, pageSize));
        result.setCurrentPage(page);
        result.setPageSize(pageSize);
        result.setTotalItems(gamesDao.countGames());
        return result;
    }

    @Override
    public void delete(int id) {
        gamesDao.delete(id);
    }

    @Override
    public int getGamePosition(int id) {
        return gamesDao.getGamePosition(id);
    }

    @Override
    public void save(Game game) {
        if (game.getId() != 0) {
            Game dbGame = gamesDao.get(game.getId());
            if (game.getName() != null) {
                dbGame.setName(game.getName());
            }
            if (game.getDescription() != null) {
                dbGame.setDescription(game.getDescription());
            }
            if (game.getImage() != null) {
                dbGame.setImage(game.getImage());
            }
            if (game.getPlays() != 0) {
                dbGame.setPlays(game.getPlays());
            }
            game = dbGame;
        }
        gamesDao.save(game);
    }

    @Override
    public Game get(int id) {
        return gamesDao.get(id);
    }

    @Override
    public void incrementPlays(int id) {
        gamesDao.incrementPlays(id);
    }

}
