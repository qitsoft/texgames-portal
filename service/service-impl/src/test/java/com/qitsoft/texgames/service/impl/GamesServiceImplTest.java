package com.qitsoft.texgames.service.impl;

import com.google.common.collect.Lists;
import com.qitsoft.texgames.dao.GamesDao;
import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.service.GamesService;
import com.qitsoft.texgames.service.model.PagedResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ssolovio on 29.12.13.
 */
@RunWith(MockitoJUnitRunner.class)
public class GamesServiceImplTest {

    private static final int PAGE_SIZE = 10;
    private static final long TOTAL_NUMBER_OF_GAMES = 20L;
    private static final int NUMBER_OF_PLAYS = 11;
    @Spy
    @InjectMocks
    private GamesService gamesService = new GamesServiceImpl();

    @Mock
    private GamesDao gamesDao;

    @Spy
    private Game game1 = new Game();

    @Spy
    private Game game2 = new Game();

    @Test
    public void testListReturnsFromDao() {
        when(gamesDao.list(anyInt(), anyInt())).thenReturn(Lists.newArrayList(game1));

        List<Game> result = gamesService.list(0, PAGE_SIZE).getItems();

        assertNotNull(result);
        assertThat(result, hasItem(equalTo(game1)));
    }

    @Test
    public void testListReturnsPagesFromDao() {
        gamesService.list(1, PAGE_SIZE);
        verify(gamesDao).list(0, PAGE_SIZE);

        gamesService.list(2, PAGE_SIZE);
        verify(gamesDao).list(PAGE_SIZE, PAGE_SIZE);
    }

    @Test
    public void testListPagedResultFields() {
        when(gamesDao.countGames()).thenReturn(TOTAL_NUMBER_OF_GAMES);
        PagedResult<Game> result = gamesService.list(1, PAGE_SIZE);
        assertEquals(1, result.getCurrentPage());
        assertEquals(PAGE_SIZE, result.getPageSize());
        assertEquals(TOTAL_NUMBER_OF_GAMES, result.getTotalItems());
        assertEquals(2, result.getTotalPages());
    }

    @Test
    public void testDeleteInvokesDirectlyDao() {
        gamesService.delete(PAGE_SIZE);
        verify(gamesDao).delete(eq(PAGE_SIZE));
    }

    @Test
    public void testGetGamePositionInvokesDaoDirectly() {
        gamesService.getGamePosition(2);
        verify(gamesDao).getGamePosition(eq(2));
    }

    @Test
    public void testSaveGameInvokesDao() {
        gamesService.save(game1);
        verify(gamesDao).save(eq(game1));
    }

    @Test
    public void testSaveExistingUpdatesByTemplate() {
        game1.setId(PAGE_SIZE);
        game1.setName("test");

        game2.setId(PAGE_SIZE);
        game2.setName("old test name");
        game2.setDescription("desc");
        game2.setPlays(NUMBER_OF_PLAYS);
        game2.setImage("image");

        when(gamesDao.get(eq(PAGE_SIZE))).thenReturn(game2);

        gamesService.save(game1);

        ArgumentCaptor<Game> captureGame = ArgumentCaptor.forClass(Game.class);
        verify(gamesDao).save(captureGame.capture());

        assertEquals("desc", captureGame.getValue().getDescription());
        assertEquals("test", captureGame.getValue().getName());
        assertEquals("image", captureGame.getValue().getImage());
        assertEquals(NUMBER_OF_PLAYS, captureGame.getValue().getPlays());
    }

    @Test
    public void testGetGameInvokesDao() {
        when(gamesDao.get(eq(PAGE_SIZE))).thenReturn(game1);
        assertEquals(game1, gamesService.get(PAGE_SIZE));

        verify(gamesDao).get(eq(PAGE_SIZE));
    }
}
