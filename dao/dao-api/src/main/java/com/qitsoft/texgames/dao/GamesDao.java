package com.qitsoft.texgames.dao;

import com.qitsoft.texgames.model.Game;

import java.util.List;

/**
 * Created by ssolovio on 29.12.13.
 */
public interface GamesDao extends BaseDao<Game> {

    List<Game> list(int offset, int limit);

    long countGames();

    int getGamePosition(int id);

    void incrementPlays(int id);

}
