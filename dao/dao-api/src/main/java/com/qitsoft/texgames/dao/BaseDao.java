package com.qitsoft.texgames.dao;

/**
 * Created by ssolovio on 07.01.14.
 */
public interface BaseDao<T> {

    T get(Number id);

    void update(T entity);

    void delete(Number id);

    void delete(T entity);

    void save(T entity);
}
