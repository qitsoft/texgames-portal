package com.qitsoft.texgames.dao.impl;

import com.qitsoft.texgames.dao.GamesDao;
import com.qitsoft.texgames.model.Game;
import com.qitsoft.texgames.model.QGame;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

/**
 * Created by ssolovio on 29.12.13.
 */
@Service
public class GamesDaoImpl extends BaseDaoImpl<Game> implements GamesDao {

    @Override
    public List<Game> list(int offset, int limit) {
        return select().from(QGame.game)
                .orderBy(QGame.game.plays.desc())
                .limit(limit)
                .offset(offset)
                .list(QGame.game);
    }

    @Override
    public long countGames() {
        return select().from(QGame.game)
                .uniqueResult(QGame.game.count());
    }

    @Override
    public int getGamePosition(int id) {
        List<Game> list = select().from(QGame.game)
                .orderBy(QGame.game.plays.desc())
                .list(QGame.game);

        Iterator<Game> iterator = list.iterator();
        int i = 1;
        while (iterator.hasNext()) {
            if (iterator.next().getId() == id) {
                break;
            }
            i++;
        }
        return i;
    }

    @Override
    public synchronized void incrementPlays(int id) {
        Game game = get(id);
        game.setPlays(game.getPlays() + 1);
        save(game);
    }

    public void delete(int id) {
        Game game = get(id);
        delete(game);
        flush();
    }
}
