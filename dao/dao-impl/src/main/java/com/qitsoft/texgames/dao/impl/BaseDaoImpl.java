package com.qitsoft.texgames.dao.impl;

import com.mysema.query.jpa.hibernate.HibernateDeleteClause;
import com.mysema.query.jpa.hibernate.HibernateQuery;
import com.mysema.query.jpa.hibernate.HibernateUpdateClause;
import com.mysema.query.types.EntityPath;
import com.qitsoft.texgames.dao.BaseDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;

/**
 * Created by ssolovio on 06.01.14.
 */
public abstract class BaseDaoImpl<T> implements BaseDao<T> {

    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> type;

    @SuppressWarnings("unchecked")
    protected Class getType() {
        if (type == null) {
            type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        }
        return type;
    }

    protected Session session() {
        return sessionFactory.getCurrentSession();
    }

    protected HibernateUpdateClause update(EntityPath<?> entity) {
        return new HibernateUpdateClause(session(), entity);
    }

    protected HibernateDeleteClause delete(EntityPath<?> entity) {
        return new HibernateDeleteClause(session(), entity);
    }

    protected HibernateQuery select() {
        return new HibernateQuery(session());
    }

    protected void flush() {
        session().flush();
    }

    protected void updateAndFlush(T entity) {
        update(entity);
        flush();
    }

    public void update(T entity) {
        session().update(entity);
    }

    @SuppressWarnings("unchecked")
    public T get(Number id) {
        return (T) session().get(getType(), id);
    }

    public void delete(Number id) {
        session().delete(get(id));
    }

    public void delete(T entity) {
        session().delete(entity);
    }

    @Override
    public void save(T entity) {
        session().saveOrUpdate(entity);
    }
}
