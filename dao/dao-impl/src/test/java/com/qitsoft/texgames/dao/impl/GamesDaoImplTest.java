package com.qitsoft.texgames.dao.impl;

import com.google.common.collect.Iterables;
import com.mysema.query.jpa.hibernate.HibernateQuery;
import com.qitsoft.texgames.model.QGame;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ssolovio on 07.01.14.
 */
@ContextConfiguration(locations = "classpath:rootDaoTestsContext.xml")
@TransactionConfiguration(transactionManager = "testTransactionManager", defaultRollback = false)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class GamesDaoImplTest extends BaseDaoTest {

    @Test
    public void emptyTest() {
        System.out.println(Iterables.toString(new HibernateQuery(getSession()).from(QGame.game).list(QGame.game)));
    }
}
