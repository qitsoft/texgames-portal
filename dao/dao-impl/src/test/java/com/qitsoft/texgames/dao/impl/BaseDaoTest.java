package com.qitsoft.texgames.dao.impl;

import liquibase.Liquibase;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;

/**
 * Created by ssolovio on 08.01.14.
 */
public abstract class BaseDaoTest {

    @Autowired
    private DataSource testDataSource;

    @Autowired
    private SessionFactory testSessionFactory;

    @Before
    public void setUpDatabase() throws Exception {
        DatabaseConnection databaseConnection = new JdbcConnection(testDataSource.getConnection());
        ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor(getClass().getClassLoader());
        Liquibase liquibase = new Liquibase("master.xml", resourceAccessor, databaseConnection);

        liquibase.dropAll();
        liquibase.update("test");

        liquibase.getDatabase().close();
    }

    public SessionFactory getTestSessionFactory() {
        return testSessionFactory;
    }

    public Session getSession() {
        return testSessionFactory.getCurrentSession();
    }
}
